import SidePanel, { PanelConfigType } from "components/SidePanel";
import { noop } from "lodash";
import {
  createContext,
  FunctionComponent,
  PropsWithChildren,
  ReactNode,
  useCallback,
  useContext,
  useMemo,
} from "react";
import { useSetState } from "react-use";

type SidePanelContextType = {
  openPanel: (panelToOpen: ReactNode, config: PanelConfigType) => void;
  closePanel: () => void;
  updateConfig: (config: PanelConfigType) => void;
  isPanelOpen: boolean;
};

const SidePanelContext = createContext<SidePanelContextType>({
  openPanel: noop,
  closePanel: noop,
  updateConfig: noop,
  isPanelOpen: false,
});

type State = {
  panel: ReactNode | null;
  panelConfig: PanelConfigType | null;
  isOpen: boolean;
};

const SidePanelProvider: FunctionComponent<PropsWithChildren<unknown>> = ({
  children,
}) => {
  const [{ panel, panelConfig, isOpen }, setState] = useSetState<State>({
    panel: null,
    panelConfig: null,
    isOpen: false,
  });

  const closePanel = useCallback(() => {
    setState((prevState) => {
      if (prevState.panelConfig?.onClose) {
        prevState.panelConfig.onClose();
      }
      return {
        isOpen: false,
      };
    });
    setTimeout(() => setState({ panel: null, panelConfig: null }), 500);
  }, [setState]);

  const openPanel = useCallback(
    async (panelToOpen: ReactNode, config: PanelConfigType) => {
      setState({
        panel: panelToOpen,
        panelConfig: config,
        isOpen: true,
      });
    },
    [setState]
  );

  const updateConfig = useCallback((config: PanelConfigType) => setState((prevState) => ({
    panelConfig: {
      ...prevState.panelConfig,
      ...config,
    },
  })), [setState]);

  const providerValue = useMemo(
    () => ({
      openPanel,
      closePanel,
      updateConfig,
      isPanelOpen: isOpen,
    }),
    [updateConfig, closePanel, isOpen, openPanel]
  );

  return (
    <SidePanelContext.Provider value={providerValue}>
      <SidePanel isOpen={isOpen} config={panelConfig} closePanel={closePanel}>
        {panel}
      </SidePanel>
      {children}
    </SidePanelContext.Provider>
  );
};

export const useSidePanel: () => SidePanelContextType = () =>
  useContext(SidePanelContext);

export default SidePanelProvider;
