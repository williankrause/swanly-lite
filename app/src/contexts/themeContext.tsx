import { noop } from "lodash";
import {
  createContext,
  FunctionComponent,
  PropsWithChildren,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";

export enum ThemeMode {
  DARK = "dark",
  LIGHT = "light",
}

type ThemeContextType = {
  mode: ThemeMode;
  toggleMode: () => void;
};

const ThemeContext = createContext<ThemeContextType>({
  mode: ThemeMode.LIGHT,
  toggleMode: noop,
});

const ThemeProvider: FunctionComponent<PropsWithChildren<unknown>> = ({
  children,
}) => {
  const [mode, setMode] = useState(ThemeMode.LIGHT);

  const toggleMode = useCallback(
    () =>
      setMode((current) =>
        current === ThemeMode.LIGHT ? ThemeMode.DARK : ThemeMode.LIGHT
      ),
    []
  );

  const providerValue = useMemo(
    () => ({
      mode,
      toggleMode,
    }),
    [mode, toggleMode]
  );

  return (
    <ThemeContext.Provider value={providerValue}>
      {children}
    </ThemeContext.Provider>
  );
};

export const useTheme: () => ThemeContextType = () => useContext(ThemeContext);

export default ThemeProvider;
