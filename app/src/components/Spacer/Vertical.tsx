import styled from '@emotion/styled';
import { FunctionComponent } from 'react';

type VerticalSpacerProps = {
    height: number;
    width?: number;
};

const Container = styled.div<VerticalSpacerProps>`
    min-height: ${({ height }) => height}px;
    ${({ width }) => width && `min-width: ${width}px;`}
`;

const VerticalSpacer: FunctionComponent<VerticalSpacerProps> = (props) => <Container {...props} />;

export default VerticalSpacer;
