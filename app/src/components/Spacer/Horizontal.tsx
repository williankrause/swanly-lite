import styled from '@emotion/styled';
import { FunctionComponent } from 'react';

type HorizontalSpacerProps = {
    width: number;
};

const Container = styled.div<HorizontalSpacerProps>`
    width: ${({ width }) => width}px;
`;

const HorizontalSpacer: FunctionComponent<HorizontalSpacerProps> = (props) => <Container {...props} />;

export default HorizontalSpacer;
