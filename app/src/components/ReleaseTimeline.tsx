import {
  FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import { invoke } from "@forge/bridge";
import Timeline from "lib/timeline";
import {
  DataPool,
  ExpandableDataItem,
  OnZoomChangeFunction,
} from "lib/timeline/types/base";
import { Project, Release } from "types";
import { dateAsUTCDate } from "utils/date";
import SwanlyLoading from "./SwanlyLoading";
import { ThemeMode, useTheme } from "contexts/themeContext";
import { useSidePanel } from "contexts/sidePanelContext";
import ReleaseDetailsPanel from "./ReleaseDetails/ReleaseDetailsPanel";
import { noop } from "lodash";
import Toolbar from "./Toolbar/Toolbar";
import { useTimelineEvents } from "lib/timeline/contexts/timeline";
import { DEFAULT_ZOOM } from "lib/timeline/constants";

type ReleaseTimelineState = {
  project: Project;
  releases: Release[];
};

const ReleaseTimeline: FunctionComponent<unknown> = () => {
  const [data, setData] = useState<ReleaseTimelineState | null>(null);
  const [zoomLevel, setZoomLevel] = useState(DEFAULT_ZOOM);
  const { mode } = useTheme();

  const { openPanel } = useSidePanel();

  const { onMoveToToday } = useTimelineEvents();

  const onZoomChange = useCallback<OnZoomChangeFunction>((newLevel) => {
    setZoomLevel(newLevel);
  }, []);

  const openReleasePanel = useCallback(
    (item: ExpandableDataItem) => {
      openPanel(<ReleaseDetailsPanel releaseId={item.id} />, {});
    },
    [openPanel]
  );

  const isDarkMode = useMemo(() => mode === ThemeMode.DARK, [mode]);

  const loadData = useCallback(async () => {
    const apiData = (await invoke("getReleases")) as ReleaseTimelineState;
    setData(apiData);
  }, []);

  useEffect(() => {
    if (!data) {
      loadData();
    }
  }, [data, loadData]);

  const timelineData = useMemo<DataPool[]>(() => {
    if (!data) {
      return [];
    }

    const { project, releases } = data;
    return [
      {
        id: project.id,
        text: project.name,
        icon: project.avatarUrls["24x24"],
        items: releases
          .filter(({ startDate, releaseDate }) => startDate || releaseDate)
          .map((release) => ({
            id: release.id,
            text: release.name,
            drawStart: dateAsUTCDate(release.startDate || release.releaseDate),
            drawEnd: dateAsUTCDate(release.releaseDate || release.startDate),
          })),
      },
    ];
  }, [data]);

  if (data === null) {
    return <SwanlyLoading />;
  }

  return (
    <>
      <Timeline
        data={timelineData}
        loadMore={noop}
        isDarkMode={isDarkMode}
        onItemNameClick={openReleasePanel}
        zoomLevel={zoomLevel}
      />
      <Toolbar
        onMoveToToday={onMoveToToday}
        onZoomChange={onZoomChange}
        zoomLevel={zoomLevel}
      />
    </>
  );
};

export default ReleaseTimeline;
