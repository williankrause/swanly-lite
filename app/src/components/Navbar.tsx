import { FunctionComponent } from "react";
import styled from "@emotion/styled";
import SwanlyLogo from "./images/SwanlyLogo";
import HorizontalSpacer from "./Spacer/Horizontal";

const Logo = styled(SwanlyLogo)`
  width: 32px;
  height: 32px;
`;

const Group = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Container = styled.div`
  height: 48px;
  padding: 0 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #fafbfc;
`;

const Title = styled.h3`
  margin-top: 0;
`;

const Navbar: FunctionComponent<unknown> = () => (
  <Container>
    <Group>
      <Logo />
      <HorizontalSpacer width={8} />
      <Title>Swanly Lite</Title>
    </Group>
  </Container>
);

export default Navbar;
