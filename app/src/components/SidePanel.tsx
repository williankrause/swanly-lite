import { FunctionComponent, PropsWithChildren, ReactNode } from 'react';
import styled from '@emotion/styled';
import CrossIcon from '@atlaskit/icon/glyph/cross';
import HorizontalSpacer from 'components/Spacer/Horizontal';
import TooltipButton from 'components/TooltipButton';

export type EmptyDivProps = {
    height?: number;
    visible: boolean;
    width?: number;
    flexDirection?: 'row' | 'column';
};

type ExpandableContainerProps = {
    isOpen?: boolean;
};

export const ExpandableContainer = styled.div<ExpandableContainerProps>`
    position: fixed;
    top: 48px;
    right: 0;
    bottom: 0;
    width: 400px;
    margin-right: ${({ isOpen }) => (isOpen ? 0 : '-438px')};
    padding: 16px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.16);
    z-index: 100;
    transition: all 0.5s cubic-bezier(0.895, 0.03, 0.685, 0.22);
    transition-property: margin-right, width;
    overflow-x: hidden;
    overflow-y: overlay;
    background-color: #ffffff;
`;

export const Container = styled.div`
    position: absolute;
    right: 0;
    padding-right: 16px;
    width: 400px;
    display: flex;
    flex-direction: column;
    z-index: 110;
    transition: all 0.1s ease-in-out;
    overflow: hidden;
    min-height: calc(100% - 48px);
`;

export const TitleRow = styled.div`
    display: flex;
    flex-direction: row;
    flex-grow: 0;
    justify-content: space-between;
    align-items: center;
    padding-bottom: 16px;
`;

type TitleProps = {
    fontSize?: number;
};
export const Title = styled.span<TitleProps>`
    font-size: ${({ fontSize }) => fontSize || 18}px;
    color: #172B4D;
`;

export const ActionButtons = styled.div`
    display: flex;
    flex-direction: row;
    flex-grow: 0;
`;

type CloseButtonProps = {
    onClick: () => void;
};

export const CloseButton: FunctionComponent<CloseButtonProps> = ({ onClick }) => {
    return (
        <TooltipButton
            tooltipContent="Close"
            tooltipPosition="top"
            onClick={onClick}
            appearance="subtle"
            iconBefore={<CrossIcon label="Close" />}
        />
    );
};

export type PanelConfigType = {
    fullControl?: boolean;
    title?: string;
    actions?: ReactNode;
    onClose?: () => void;
};

type SidePanelProps = {
    config: PanelConfigType | null;
    isOpen: boolean;
    closePanel: () => void;
};

type SidePanelActionsProps = {
    actions?: ReactNode;
    closePanel: () => void;
};

export const SidePanelActions: FunctionComponent<SidePanelActionsProps> = ({
    actions,
    closePanel,
}) => (
    <ActionButtons>
        {actions && (
            <>
                {actions}
                <HorizontalSpacer width={16} />
            </>
        )}
        <CloseButton onClick={closePanel} />
    </ActionButtons>
);

const SidePanel: FunctionComponent<PropsWithChildren<SidePanelProps>> = ({
    children,
    config,
    isOpen,
    closePanel,
}) => {
    return (
        <ExpandableContainer isOpen={isOpen}>
            {config?.fullControl ? (
                children
            ) : (
                <>
                    <Container>
                        {config && (
                            <>
                                <TitleRow>
                                    <Title>{config.title}</Title>
                                    <SidePanelActions
                                        actions={config.actions}
                                        closePanel={closePanel}
                                    />
                                </TitleRow>
                                {children}
                            </>
                        )}
                    </Container>
                </>
            )}
        </ExpandableContainer>
    );
};

export default SidePanel;
