import styled from '@emotion/styled';
import { FunctionComponent } from 'react';
import SwimmingSwan from './images/SwimmingSwan';

const Container = styled.div`
    height: 800px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

const SwanlyLoading: FunctionComponent<unknown> = () => (
    <Container>
        <SwimmingSwan />
    </Container>
);

export default SwanlyLoading;
