import styled from '@emotion/styled';
import { FunctionComponent } from 'react';

const Container = styled.div`
    width: 200px;
    height: 200px;
    border-radius: 50%;
    border: 1px solid (black, 0.2);
    box-shadow: 5px 5px 15px 5px rgba(black, 0.2);
    position: relative;
    overflow: hidden;
    background: linear-gradient(113deg, #1c1c1c, #333333) no-repeat;
`;

const Swan = styled.div`
    width: 100px;
    padding-top: 50px;
    padding-left: 45px;
    position: absolute;
    z-index: 10000;
    animation-name: swan;
    animation-duration: 4s;
    animation-iteration-count: infinite;
`;

const Wave1 = styled.div`
    padding-top: 110px;
    width: 500px;
    position: relative;
    animation-name: wave;
    animation-duration: 15s;
    animation-iteration-count: infinite;
    opacity: 0.1;
`;

const Wave2 = styled.div`
    width: 600px;
    position: relative;
    animation-name: wave2;
    animation-duration: 20s;
    animation-iteration-count: infinite;
    opacity: 0.1;
`;

const Wave3 = styled.div`
    width: 600px;
    position: relative;
    animation-name: wave3;
    animation-duration: 10s;
    animation-iteration-count: infinite;
    opacity: 0.1;
`;

const WaveSvg = () => (
    <svg className="waves" viewBox="0 24 150 28" preserveAspectRatio="none" shapeRendering="auto">
        <defs>
            <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
        </defs>
        <g className="parallax">
            <use xlinkHref="#gentle-wave" x="0" y="0" fill="rgba(255,255,255,0.7" />
            <use xlinkHref="#gentle-wave" x="15" y="5" fill="rgb(255,255,255)" />
        </g>
    </svg>
);

const SwimmingSwan: FunctionComponent<unknown> = () => (
    <Container>
        <Swan>
            <svg viewBox="0 0 30.651 30.661">
                <g transform="translate(-6 -5.929)">
                    <path
                        d="M13,19l3.406,8.514,13.623,2.554-3.406-5.96Z"
                        transform="translate(-0.771 -0.918)"
                        fill="#727272"
                    />
                    <path
                        d="M44.952-30.5,50.8-3.082l5.947-7.509L46.44-33.1Z"
                        transform="translate(-20.094 39.519)"
                        fill="#727272"
                    />
                    <path
                        d="M-8.6,25.654l-4.377-1.577-8.985,1.533,2.409,5.288L-27.811,32.6l9.21,5.677,11.134-8.067Z"
                        transform="translate(36.656 -1.69)"
                        fill="#515151"
                    />
                    <path
                        d="M39.136-18.388,38.7-13.733l2.864.632Z"
                        transform="translate(-12.798 24.813)"
                        fill="#515151"
                    />
                    <path
                        d="M30.242,28.676l.665,1.862,9.346,1.819Z"
                        transform="translate(-3.602 -3.43)"
                        fill="#515151"
                    />
                    <path d="M6,26,17.92,39.623H30.691l5.96-7.663Z" transform="translate(0 -3.033)" fill="#b6b6b6" />
                    <path
                        d="M40.629,28.92l-1.073,2.357,5.139.376Z"
                        transform="translate(-8.178 -20.402)"
                        fill="#727272"
                    />
                    <path d="M20,40.663,38.731,33l-5.96,7.663Z" transform="translate(-2.08 -4.073)" fill="#727272" />
                    <path
                        d="M30.459,29.418l.459,4.229,4.677.232,1.5-3.426-2.426-1.524Z"
                        transform="translate(-4.121 -23)"
                        fill="#828282"
                    />
                </g>
            </svg>
        </Swan>
        <Wave1>
            <WaveSvg />
        </Wave1>
        <Wave2>
            <WaveSvg />
        </Wave2>
        <Wave3>
            <WaveSvg />
        </Wave3>
    </Container>
);

export default SwimmingSwan;
