import { FunctionComponent } from 'react';

type SwanlyLogoProps = {
    className?: string;
};

const SwanlyLogo: FunctionComponent<SwanlyLogoProps> = ({ className }) => (
    <svg viewBox="0 0 2418.387 2431.603" className={className}>
        <g transform="translate(-6 -11.617)">
            <path
                d="M13,19,281.709,705.177,1356.548,911.03l-268.71-480.324Z"
                transform="translate(484.446 932.649)"
                fill="#333"
            />
            <path
                d="M44.952,176.418,506.221,2385.881l469.21-605.2L162.337-33.1Z"
                transform="translate(1448.955 44.98)"
                fill="#333"
            />
            <path
                d="M1488.171,151.205,1142.858,24.076,433.949,147.634l190.1,426.181L-27.811,711.227l726.64,457.547L1577.29,518.657Z"
                transform="translate(258.312 1274.421)"
                fill="#1c1c1c"
            />
            <path
                d="M73.325-18.388,38.7,356.743l220.989,39.718Z"
                transform="translate(1537.316 30.523)"
                fill="#1c1c1c"
            />
            <path
                d="M30.242,28.676l52.482,150.1L820.1,325.394Z"
                transform="translate(1604.285 1500.266)"
                fill="#1c1c1c"
            />
            <path
                d="M6,26,946.483,1123.883H1954.144l470.242-617.559Z"
                transform="translate(0 1319.336)"
                fill="#828282"
            />
            <path d="M124.2,28.92,39.556,218.881l405.462,30.3Z" transform="translate(1968.755 151.982)" fill="#333" />
            <path d="M20,650.559,1497.9,33,1027.661,650.559Z" transform="translate(926.483 1792.66)" fill="#333" />
            <path
                d="M30.459,34.617,66.694,375.4l369,18.7,118.187-276.1L329.759,34.617Z"
                transform="translate(1580.238 -23)"
                fill="#828282"
            />
        </g>
    </svg>
);

export default SwanlyLogo;
