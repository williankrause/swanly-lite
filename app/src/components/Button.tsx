import { ButtonProps, CustomThemeButton } from '@atlaskit/button';
import { useTheme } from 'contexts/themeContext';
import { FunctionComponent } from 'react';

const Button: FunctionComponent<ButtonProps> = (props) => {
    const { mode } = useTheme();

    return <CustomThemeButton {...props} theme={(theme, otherProps) => theme({ ...otherProps, mode })} />
}

export default Button;