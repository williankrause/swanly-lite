import styled from "@emotion/styled";
import { FunctionComponent } from "react";
import VerticalSpacer from "./Spacer/Vertical";

const Container = styled.div`
  background-color: #cdcdcd;
  width: 300px;
  height: 18px;
  border-radius: 5px;
  margin-right: 10px;
  animation: placeholder-pulse 1.5s infinite;
`;

type PlaceholderProps = {
  className?: string;
};

const Placeholder: FunctionComponent<PlaceholderProps> = ({
  className = "",
}) => (
  <>
    <Container className={className} />
    <VerticalSpacer height={8} />
    <Container className={className} />
    <VerticalSpacer height={8} />
    <Container className={className} />
    <VerticalSpacer height={8} />
    <Container className={className} />
  </>
);

export default Placeholder;
