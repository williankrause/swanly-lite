import styled from "@emotion/styled";
import {
  TabList as UnstyledTabList,
  Tabs as UnstyledTabs,
  Tab as UnstyledTab,
  TabPanel as UnstyledTabPanel,
} from "react-tabs";

const TabList = styled(UnstyledTabList)`
  margin: 0;
  padding: 0;
  background-color: #f4f5f7;
  border: none;
`;

const Tabs = styled(UnstyledTabs)`
  -webkit-tap-highlight-color: transparent;
  color: #42526e;
`;

const Tab = styled(UnstyledTab)`
  display: inline-block;
  border: none;
  position: relative;
  list-style: none;
  padding: 6px 9px;
  margin: 4px 8px 0px 8px;
  cursor: pointer;

  &.react-tabs__tab--selected {
    color: #0052cc;
  }

  &.react-tabs__tab--disabled {
    color: GrayText;
    cursor: default;
  }

  &.react-tabs__tab:focus {
    box-shadow: none;
    border-color: none;
    outline: none;
  }

  &.react-tabs__tab:focus:after {
    content: "";
    position: absolute;
    height: 5px;
    left: -4px;
    right: -4px;
    bottom: -5px;
  }
`;

const TabPanel = styled(UnstyledTabPanel)`
  display: none;
  margin: 12px 8px 12px 8px;

  &.react-tabs__tab-panel--selected {
    display: block;
  }
`;

interface ExtraProp {
  tabsRole: string;
}

(Tab as typeof Tab & ExtraProp).tabsRole = "Tab";
(Tabs as typeof Tabs & ExtraProp).tabsRole = "Tabs";
(TabPanel as typeof TabPanel & ExtraProp).tabsRole = "TabPanel";
(TabList as typeof TabList & ExtraProp).tabsRole = "TabList";

export { Tab, TabList, Tabs, TabPanel };
