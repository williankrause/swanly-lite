import styled from "@emotion/styled";
import FieldLabel from "components/FieldLabel";
import { FunctionComponent } from "react";

const ReadViewContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  min-height: 20px;
  word-break: break-word;
  color: #172b4d;
`;

type ReleaseDescriptionProps = {
  text: string;
};

const ReleaseDescription: FunctionComponent<ReleaseDescriptionProps> = ({
  text,
}) => (
  <ReadViewContainer>
    <FieldLabel>Description</FieldLabel>
    {text}
  </ReadViewContainer>
);

export default ReleaseDescription;
