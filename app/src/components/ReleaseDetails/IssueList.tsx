import styled from "@emotion/styled";
import { FunctionComponent, useCallback } from "react";
import {
  Assignee,
  Issue,
  IssuePriority,
  IssueStatus,
  StatusCategoryType,
} from "types";
import { ObjectResult } from "@atlaskit/quick-search";
import Lozenge, { ThemeAppearance } from "@atlaskit/lozenge";
import { router } from "@forge/bridge";

const IssuesListObjectWrapper = styled.div`
  border-top: solid 0.5px #f4f5f7;
  border-bottom: solid 0.5px #f4f5f7;
  position: relative;
  > a {
    color: #091e42 !important;
  }
`;

type IssueKeyTextProps = {
  isDone: boolean;
};

const IssueKeyText = styled.span<IssueKeyTextProps>`
  text-decoration: ${({ isDone }) => (isDone ? "line-through" : "none")};
  color: #5e6c84;
  font-size: 12px;
  white-space: nowrap;
  padding-right: 8px;
`;

const PriorityIcon = styled.img`
  height: 13px;
  vertical-align: -2px;
  margin-right: 7px;
  display: inline-block;
`;

const AssigneeIcon = styled.img`
  object-fit: cover;
  border-radius: 50%;
  width: 18px;
  height: 18px;
  vertical-align: -4px;
  margin-right: 7px;
  display: inline-block;
`;

const LozengeWrapper = styled.div`
  vertical-align: middle;
  display: inline-block;
  margin-top: -4px;
`;

// @ts-ignore
const LozengeStyled = styled(Lozenge)`
  font-weight: normal !important;
`;

type ObjectResultAfterProps = {
  status: IssueStatus;
  priority: IssuePriority;
  assignee: Assignee;
};

const ObjectResultAfter: FunctionComponent<ObjectResultAfterProps> = ({
  status,
  priority,
  assignee,
}) => {
  let appearance: ThemeAppearance = "default";
  if (status?.statusCategory?.key === "indeterminate") {
    appearance = "inprogress";
  } else if (status?.statusCategory?.key === "done") {
    appearance = "success";
  }
  return (
    <div>
      {priority && priority.iconUrl && <PriorityIcon src={priority.iconUrl} />}
      {assignee && assignee.avatarUrls && (
        <AssigneeIcon src={assignee.avatarUrls["24x24"]} />
      )}
      <LozengeWrapper>
        {/* @ts-ignore */}
        <LozengeStyled appearance={appearance}>{status?.name}</LozengeStyled>
      </LozengeWrapper>
    </div>
  );
};

type IssueListProps = {
  issues: Issue[];
};

const IssueList: FunctionComponent<IssueListProps> = ({ issues }) => {
  const goToIssue = useCallback((key: string) => router.open(`/browse/${key}`), []);

  return (
    <>
      {issues.length === 0 && <p>No issues to show</p>}
      {issues.length > 0 &&
        issues.map((issue) => (
          <IssuesListObjectWrapper key={issue.id}>
            <ObjectResult
              key={issue.id}
              resultId={issue.id || ""}
              name={
                <div>
                  <IssueKeyText
                    isDone={
                      issue?.fields?.status?.statusCategory?.key ===
                      StatusCategoryType.DONE
                    }
                  >
                    {issue.key}
                  </IssueKeyText>{" "}
                  {issue?.fields?.summary}
                </div>
              }
              avatarUrl={issue?.fields?.issuetype?.iconUrl || ""}
              elemAfter={
                <ObjectResultAfter
                  status={issue?.fields?.status}
                  priority={issue?.fields?.priority}
                  assignee={issue?.fields?.assignee}
                />
              }
              onClick={() => goToIssue(issue.key)}
            />
          </IssuesListObjectWrapper>
        ))}
    </>
  );
};

export default IssueList;
