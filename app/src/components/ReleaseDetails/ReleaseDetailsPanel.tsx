import styled from "@emotion/styled";
import { invoke } from "@forge/bridge";
import Placeholder from "components/Placeholder";
import VerticalSpacer from "components/Spacer/Vertical";
import { useSidePanel } from "contexts/sidePanelContext";
import { FunctionComponent, useCallback, useEffect, useState } from "react";
import { Release } from "types";
import ReleaseDates from "./ReleaseDates";
import ReleaseDescription from "./ReleaseDescription";
import ReleasePanelTabs from "./ReleasePanelTabs";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

type ReleaseDetailsPanelProps = {
  releaseId: string;
};

const ReleaseDetailsPanel: FunctionComponent<ReleaseDetailsPanelProps> = ({
  releaseId,
}) => {
  const [release, setRelease] = useState<Release | null>(null);
  const { updateConfig } = useSidePanel();

  const loadData = useCallback(async () => {
    const releaseData = (await invoke("getRelease", {
      id: releaseId,
    })) as Release;
    setRelease(releaseData);
    updateConfig({ title: releaseData.name });
  }, [releaseId, updateConfig]);

  useEffect(() => {
    if (release === null) {
      // If release is undefined, it means the user can't access it
      loadData();
    }
    if (release?.id !== releaseId) {
      setRelease(null);
    }
  }, [loadData, release, releaseId]);

  if (release === null) {
    return <Placeholder />;
  }

  return (
    <Container>
      <ReleaseDescription text={release.description} />
      <VerticalSpacer height={8} />
      <ReleaseDates
        start={release.userStartDate}
        end={release.userReleaseDate}
      />
      <VerticalSpacer height={16} />
      <ReleasePanelTabs
        scope={release.scope || []}
        affected={release.affected || []}
      />
    </Container>
  );
};

export default ReleaseDetailsPanel;
