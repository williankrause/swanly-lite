import styled from "@emotion/styled";
import TextField from "@atlaskit/textfield";
import { FunctionComponent } from "react";
import HorizontalSpacer from "components/Spacer/Horizontal";
import FieldLabel from "components/FieldLabel";

const Container = styled.div`
    display: flex;
    flex-direction: row;
    flex-grow: 1;
    align-items: center;
`;

const DateContainer = styled.div`
    display: flex;
    flex-direction: column;
`;

type DateFieldProps = {
    date: string;
    label: string;
}

const DateField: FunctionComponent<DateFieldProps> = ({ date, label }) => (
    <DateContainer>
        <FieldLabel>{label}</FieldLabel>
        <TextField value={date} isReadOnly />
    </DateContainer>
)

type ReleaseDatesProps = {
    start: string;
    end: string;
}

const ReleaseDates: FunctionComponent<ReleaseDatesProps> = ({ start, end }) => (
    <Container>
        <DateField date={start} label="Start date" />
        <HorizontalSpacer width={8} />
        <DateField date={end} label="End date" />
    </Container>
);

export default ReleaseDates;