import { TabList, Tabs, Tab, TabPanel } from "components/Tabs";
import { FunctionComponent } from "react";
import { Issue } from "types";
import IssueList from "./IssueList";

type ReleasePanelTabsProps = {
  scope: Issue[];
  affected: Issue[];
};

const ReleasePanelTabs: FunctionComponent<ReleasePanelTabsProps> = ({
  scope,
  affected,
}) => (
  <Tabs>
    <TabList>
      <Tab>Issues in version</Tab>
      <Tab>Affects version</Tab>
    </TabList>

    <TabPanel>
      <IssueList issues={scope} />
    </TabPanel>
    <TabPanel>
      <IssueList issues={affected} />
    </TabPanel>
  </Tabs>
);

export default ReleasePanelTabs;
