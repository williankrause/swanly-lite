import { FunctionComponent, ReactNode } from 'react';
import { ButtonProps } from '@atlaskit/button';
import Tooltip from '@atlaskit/tooltip';
import { TriggerProps } from '@atlaskit/popup';
import Button from 'components/Button';

type TooltipButtonProps = ButtonProps & {
    tooltipContent: ReactNode;
    tooltipPosition?:
        | 'bottom'
        | 'left'
        | 'right'
        | 'top'
        | 'auto'
        | 'auto-start'
        | 'auto-end'
        | 'top-start'
        | 'top-end'
        | 'bottom-start'
        | 'bottom-end'
        | 'right-start'
        | 'right-end'
        | 'left-start'
        | 'left-end'
        | 'mouse';
    triggerProps?: TriggerProps;
};

const TooltipButton: FunctionComponent<TooltipButtonProps> = ({
    tooltipContent,
    tooltipPosition,
    triggerProps = {},
    ...props
}) => (
    <Tooltip content={tooltipContent} position={tooltipPosition}>
        <Button {...triggerProps} {...props} />
    </Tooltip>
);

export default TooltipButton;
