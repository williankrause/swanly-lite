import { FunctionComponent, PropsWithChildren } from 'react';
import styled from '@emotion/styled';

const Container = styled.label`
    display: inline-block;
    font-size: 12px;
    font-style: 'inherit';
    line-height: 2;
    color: #6B778C;
    font-weight: 600;
`;

type FieldLabelProps = {
    className?: string;
};

const FieldLabel: FunctionComponent<PropsWithChildren<FieldLabelProps>> = ({ children, className = '' }) => (
    <Container className={className}>{children}</Container>
);

export default FieldLabel;
