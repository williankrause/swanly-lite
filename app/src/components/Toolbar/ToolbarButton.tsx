import React, { FunctionComponent, ReactElement } from 'react';
import Button from '@atlaskit/button';
import Tooltip from '@atlaskit/tooltip';
import { TriggerProps } from '@atlaskit/popup';
import styled from '@emotion/styled';

type ToolbarButtonProps = {
    icon: ReactElement;
    title: string;
    onClick: () => void;
    triggerProps?: TriggerProps;
};

const Container = styled.div`
    position: relative;
`;

const ToolbarThemedButton = styled(Button)`
    &:hover {
        background-color: #F4F5F7;
    }
    &:active {
        background-color: #C1C7D0;
    }
`;

const ToolbarButton: FunctionComponent<ToolbarButtonProps> = ({
    icon,
    title,
    onClick,
    triggerProps = {},
}) => (
    <Tooltip content={title} position="left">
        <Container>
            <ToolbarThemedButton
                {...triggerProps}
                shouldFitContainer
                appearance="subtle"
                iconBefore={icon}
                onClick={onClick}
                title={title}
            />
        </Container>
    </Tooltip>
);

export default ToolbarButton;
