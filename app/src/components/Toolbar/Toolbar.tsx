import styled from "@emotion/styled";
import { useSidePanel } from "contexts/sidePanelContext";
import { FunctionComponent, useCallback } from "react";
import EditorAddIcon from "@atlaskit/icon/glyph/editor/add";
import EditorDividerIcon from "@atlaskit/icon/glyph/editor/divider";
import LocationIcon from "@atlaskit/icon/glyph/location";
import ToolbarButton from "./ToolbarButton";
import {
  OnMoveToTodayFunction,
  OnZoomChangeFunction,
  ZoomLevel,
} from "lib/timeline/types/base";

type ContainerProps = {
  isPanelOpen: boolean;
};

const Container = styled.div<ContainerProps>`
  position: fixed;
  top: 108px;
  right: ${({ isPanelOpen }) => (isPanelOpen ? "448px" : "16px")};
  width: 32px;
  height: 96px;
  justify-content: space-between;
  background-color: #ffffff;
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.16);
  border: solid 1px #ebecf0;
  border-radius: 3px;
  transition: all 0.5s cubic-bezier(0.895, 0.03, 0.685, 0.22);
  display: flex;
  flex-direction: column;
`;

const ICON_SIZE = "medium";

type ToolbarProps = {
  zoomLevel: ZoomLevel;
  onZoomChange: OnZoomChangeFunction;
  onMoveToToday: OnMoveToTodayFunction;
};

const Toolbar: FunctionComponent<ToolbarProps> = ({
  zoomLevel,
  onZoomChange,
  onMoveToToday,
}) => {
  const { isPanelOpen } = useSidePanel();

  const handleZoomIn = useCallback(
    () => onZoomChange(Math.max(zoomLevel - 1, ZoomLevel.DAILY)),
    [onZoomChange, zoomLevel]
  );
  const handleZoomOut = useCallback(
    () => onZoomChange(Math.min(zoomLevel + 1, ZoomLevel.YEARLY)),
    [onZoomChange, zoomLevel]
  );

  return (
    <Container isPanelOpen={isPanelOpen}>
      <ToolbarButton
        icon={<LocationIcon size={ICON_SIZE} label="Back to Today" />}
        title="Back to Today"
        onClick={onMoveToToday}
      />
      <ToolbarButton
        icon={<EditorAddIcon size={ICON_SIZE} label="Zoom in" />}
        title="Zoom in"
        onClick={handleZoomIn}
      />
      <ToolbarButton
        icon={<EditorDividerIcon size={ICON_SIZE} label="Zoom out" />}
        title="Zoom out"
        onClick={handleZoomOut}
      />
    </Container>
  );
};

export default Toolbar;
