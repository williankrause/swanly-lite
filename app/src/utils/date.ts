// import { convertToTimeZone } from "date-fns-timezone";

export const defaultTimezone = 'UTC';

export const dateAsUTCDate = (date: Date | string | number): Date => {
    // return convertToTimeZone(date, { timeZone: defaultTimezone });
    return new Date(date);
};