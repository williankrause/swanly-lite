export type Instance = {
  baseUrl: string;
}

type AvatarUrls = {
  "16x16": string;
  "24x24": string;
  "32x32": string;
  "48x48": string;
};

export type Project = {
  avatarUrls: AvatarUrls;
  description: string;
  id: string;
  key: string;
  name: string;
};

export type Assignee = {
  accountId: string;
  avatarUrls: AvatarUrls;
  displayName: string;
};

export type IssueType = {
    id: string;
    iconUrl: string;
    name: string;
    description: string;
};

export type IssuePriority = {
    id: string;
    iconUrl: string;
    name: string;
};

export enum StatusCategoryType {
    NEW = 'new',
    INDETERMINATE = 'indeterminate',
    DONE = 'done',
};

export type IssueStatusCategory = {
    id: number;
    key: StatusCategoryType,
    name: string;
}

export type IssueStatus = {
    id: string;
    iconUrl: string;
    name: string;
    statusCategory: IssueStatusCategory;
}

export type Issue = {
  id: string;
  key: string;
  fields: {
    assignee: Assignee;
    summary: string;
    issuetype: IssueType;
    priority: IssuePriority;
    status: IssueStatus;
  };
};

export type Release = {
  archived: boolean;
  description: string;
  id: string;
  name: string;
  releaseDate: string;
  released: boolean;
  startDate: string;
  userStartDate: string;
  userReleaseDate: string;
  scope?: Issue[];
  affected?: Issue[];
};
