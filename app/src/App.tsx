import { FunctionComponent } from "react";
import Navbar from "components/Navbar";
import ReleaseTimeline from "components/ReleaseTimeline";
import ThemeProvider from "contexts/themeContext";
import SidePanelProvider from "contexts/sidePanelContext";
import { TimelineProvider } from "lib/timeline/contexts/timeline";

const App: FunctionComponent<unknown> = () => (
  <ThemeProvider>
    <SidePanelProvider>
      <TimelineProvider>
        <Navbar />
        <ReleaseTimeline />
      </TimelineProvider>
    </SidePanelProvider>
  </ThemeProvider>
);

export default App;
