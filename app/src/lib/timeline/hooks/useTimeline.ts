/* eslint-disable no-param-reassign */
import { max, min } from 'date-fns';
import { Layer } from 'konva/lib/Layer';
import { Stage } from 'konva/lib/Stage';
import { last } from 'lodash';
import { MutableRefObject, useCallback, useEffect, useMemo, useRef } from 'react';
import GridColumnImpl from '../api/Grid/GridColumn';
import GridHeaderImpl from '../api/Grid/GridHeader';
import {
    createGridLayer,
    createHeaderLayer,
    createNamesLayer,
    createPoolDividersLayer,
    createPoolsLayer,
} from '../api/konva';
import { ItemToggleFunction, NoodleNameClickFunction, NoodleToggleFunction } from '../api/SwimPool/PoolNoodle';
import { SwimPool } from '../api/SwimPool/SwimPool';
import { ANIMATION_DURATION, STAGE_PADDING, TIMELINE_HEADER_SPACING, ZOOM_LEVELS } from '../constants';
import {
    buildGrid,
    buildTodayMarker,
    moveTodayMarker,
    renderMoreColumns,
    resizeGrid,
    GridSettings,
} from '../handlers/grid';
import {
    updatePools,
    MappedPoolNoodles,
    MappedSwimPoolDividers,
    MappedSwimPools,
    resizeNoodles,
    toggleNoodle,
    toggleAllNoodles,
} from '../handlers/pool';
import {
    DataPool,
    LoadMoreFunction,
    OnScrollFunction,
    UpdateLaneIndexFunction,
    ZoomLevel,
    ZoomType,
} from '../types/base';
import SwimPoolDivider from '../types/swimPoolDivider';
import { SetLimitDatesFunction } from '../contexts/timeline';
import * as lightTheme from '../theme/light';
import * as darkTheme from '../theme/dark';
import { Theme } from '../theme';

type UseTimelineDefinitions = {
    stages: {
        main: MutableRefObject<Stage | null>;
        poolDividers: MutableRefObject<Stage | null>;
        header: MutableRefObject<Stage | null>;
        grid: MutableRefObject<Stage | null>;
    };
    events: {
        onScroll: OnScrollFunction;
        onBackToToday: () => void;
        recreateAllPools: () => void;
    };
};

const useTimeline = (
    zoomLevel: ZoomLevel,
    loadMore: LoadMoreFunction,
    data: DataPool[],
    width: number,
    height: number,
    center: number,
    backgroundImage: HTMLImageElement | undefined,
    onNoodleToggle: ItemToggleFunction,
    onNoodleNameClick: NoodleNameClickFunction,
    onUpdateLaneIndex: UpdateLaneIndexFunction,
    isExpandedMode: boolean,
    setLimitDates: SetLimitDatesFunction,
    isDarkMode: boolean,
): UseTimelineDefinitions => {
    const theme = useMemo<Theme>(() => isDarkMode ? darkTheme : lightTheme, [isDarkMode]);
    const mainStageRef = useRef<Stage | null>(null);
    const poolDividersStageRef = useRef<Stage | null>(null);
    const headerStageRef = useRef<Stage | null>(null);
    const gridStageRef = useRef<Stage | null>(null);

    const gridLayerRef = useRef<Layer | null>(null);
    const headerLayerRef = useRef<Layer | null>(null);
    const poolDividersLayerRef = useRef<Layer | null>(null);
    const poolsLayerRef = useRef<Layer | null>(null);
    const namesLayerRef = useRef<Layer | null>(null);

    const gridRef = useRef(buildGrid(zoomLevel));
    const todayMarkerRef = useRef(buildTodayMarker(zoomLevel));
    const zoomLevelRef = useRef(zoomLevel);

    const mappedPoolsRef = useRef<MappedSwimPools>({});
    const mappedDividersRef = useRef<MappedSwimPoolDividers>({});
    const mappedNoodlesRef = useRef<MappedPoolNoodles>({});

    const poolsRef = useRef<SwimPool[]>([]);
    const dividersRef = useRef<SwimPoolDivider[]>([]);

    const batchDraw = useCallback(() => {
        mainStageRef.current?.batchDraw();
        poolDividersStageRef.current?.batchDraw();
        headerStageRef.current?.batchDraw();
        gridStageRef.current?.batchDraw();
    }, []);

    const toggle = useCallback<NoodleToggleFunction>(
        (noodle) => {
            toggleNoodle(noodle, poolsRef.current, dividersRef.current);
            batchDraw();
            onNoodleToggle(noodle.item, noodle.isExpanded);
        },
        [batchDraw, onNoodleToggle],
    );

    const drawGrid = useCallback(() => {
        gridLayerRef.current?.destroy();
        gridLayerRef.current = createGridLayer(
            gridRef.current.gridColumns,
            height,
            backgroundImage,
            todayMarkerRef.current,
            theme,
            zoomLevel,
        );
        gridStageRef.current?.add(gridLayerRef.current);
        setLimitDates({ startDate: gridRef.current.startDate, endDate: gridRef.current.endDate });
    }, [backgroundImage, height, setLimitDates, theme, zoomLevel]);

    const drawHeader = useCallback(() => {
        headerLayerRef.current?.destroy();
        headerLayerRef.current = createHeaderLayer(gridRef.current.gridColumns, todayMarkerRef.current, theme);
        headerStageRef.current?.add(headerLayerRef.current);
    }, [theme]);

    const drawPoolDividers = useCallback(() => {
        poolDividersLayerRef.current = createPoolDividersLayer(
            poolDividersLayerRef.current,
            dividersRef.current,
            width,
            poolDividersStageRef.current,
            theme,
        );
    }, [theme, width]);

    const recreateAllPools = useCallback(() => {
        mainStageRef.current?.destroyChildren();
        poolDividersStageRef.current?.destroyChildren();
        poolsLayerRef.current?.destroyChildren();
        namesLayerRef.current?.destroyChildren();

        namesLayerRef.current = null;
        poolsLayerRef.current = null;
        poolDividersLayerRef.current = null;

        poolsRef.current = [];
        dividersRef.current = [];
        mappedPoolsRef.current = {};
        mappedDividersRef.current = {};
        mappedNoodlesRef.current = {};

        loadMore(gridRef.current.startDate, gridRef.current.endDate);
    }, [loadMore]);

    const drawNames = useCallback(() => {
        const pos = -(mainStageRef.current?.x() || 0) || -center;
        namesLayerRef.current = createNamesLayer(
            namesLayerRef.current,
            dividersRef.current,
            pos,
            mainStageRef.current,
            theme,
        );
    }, [center, theme]);

    const drawPools = useCallback(() => {
        poolsLayerRef.current = createPoolsLayer(
            poolsLayerRef.current,
            poolsRef.current,
            toggle,
            onNoodleNameClick,
            mainStageRef.current,
            theme,
            // we need to invert the value because of scroll direction
            -(mainStageRef.current?.x() || 0),
        );
    }, [onNoodleNameClick, theme, toggle]);

    const updateGrid = useCallback(
        (gridDefs: GridSettings) => {
            const { startDate, endDate, leftThreshold, rightThreshold, gridColumns } = gridDefs;

            gridRef.current = {
                gridColumns,
                leftThreshold,
                rightThreshold,
                startDate: min([gridRef.current.startDate, startDate]),
                endDate: max([gridRef.current.endDate, endDate]),
            };

            setLimitDates({ startDate: gridRef.current.startDate, endDate: gridRef.current.endDate });

            gridLayerRef.current?.removeChildren();
            headerLayerRef.current?.removeChildren();
            gridRef.current.gridColumns.forEach((column) => {
                if (!column.columnImpl) {
                    column.columnImpl = new GridColumnImpl(column, height, backgroundImage, theme, zoomLevel);
                }
                if (!column.headerImpl) {
                    column.headerImpl = new GridHeaderImpl(column, theme);
                }
                gridLayerRef.current?.add(column.columnImpl.group);
                headerLayerRef.current?.add(column.headerImpl.group);
            });
            if (todayMarkerRef.current.lineImpl) {
                gridLayerRef.current?.add(todayMarkerRef.current.lineImpl?.group);
            }
            if (todayMarkerRef.current.markerImpl) {
                headerLayerRef.current?.add(todayMarkerRef.current.markerImpl?.group);
            }

            loadMore(startDate, endDate);
        },
        [backgroundImage, height, loadMore, setLimitDates, theme, zoomLevel],
    );

    const checkInfiniteScroll = useCallback(
        async (deltaX: number, x: number) => {
            let gridDefs;

            // Calculates if infinite scroll should be triggered.
            // Theres a gotcha: when moving right, instead of stageX be positive, it will be negative
            // and vice versa. Because of this, we need to swap comparisons
            if (deltaX > 0) {
                if (x >= gridRef.current.rightThreshold) {
                    gridDefs = renderMoreColumns(gridRef.current.gridColumns, true, zoomLevelRef.current);
                }
            } else if (deltaX < 0) {
                // we check this to avoid checking on vertical scrolling
                if (x <= gridRef.current.leftThreshold) {
                    gridDefs = renderMoreColumns(gridRef.current.gridColumns, false, zoomLevelRef.current);
                }
            }

            // if more columns were rendered, we fetch more data
            if (gridDefs) {
                updateGrid(gridDefs);
            }
        },
        [updateGrid],
    );

    const onScroll = useCallback<OnScrollFunction>(
        (coords) => {
            // check if should scroll horizontally or vertically
            if (Math.abs(coords.deltaX) > Math.abs(coords.deltaY)) {
                const stageX = mainStageRef.current?.x() || 0;

                const newX = stageX - coords.deltaX;

                namesLayerRef.current?.x(namesLayerRef.current?.x() + coords.deltaX);

                // Moves stage horizontally
                mainStageRef.current?.x(newX);
                headerStageRef.current?.x(newX);
                gridStageRef.current?.x(newX);

                // propagates event to pools
                poolsRef.current.forEach((pool) => {
                    // we need to invert the value because of scroll direction
                    pool.impl?.onScroll(-newX);
                });

                checkInfiniteScroll(coords.deltaX, -stageX);
            } else {
                const lastPool = last(poolsRef.current);
                const lastY = (lastPool?.y || 0) + (lastPool?.height || 0) + TIMELINE_HEADER_SPACING + STAGE_PADDING;
                const difference = lastY - height;

                if (difference > 0) {
                    // Allows scrolling down only until the visible canvas end, or if scrolling up,
                    // do not allow go over 0
                    const positionAfterScroll = (mainStageRef.current?.y() || 0) - coords.deltaY;
                    const newY = Math.min(Math.max(positionAfterScroll, -difference), 0);
                    mainStageRef.current?.y(newY);
                    poolDividersStageRef.current?.y(newY);
                } else {
                    // If visible canvas height is smaller than stage height, prevent vertical scrolling
                    mainStageRef.current?.y(0);
                    poolDividersStageRef.current?.y(0);
                }
            }
            batchDraw();
        },
        [batchDraw, checkInfiniteScroll, height],
    );

    const renderMoreColumnsOnZoomChange = useCallback(
        (newX: number, newZoomLevel: ZoomLevel) => {
            const toRight = newX >= 0;

            const shouldRenderMoreColumns = toRight
                ? newX >= gridRef.current.rightThreshold
                : newX <= gridRef.current.leftThreshold;

            if (shouldRenderMoreColumns) {
                const newGridDefs = renderMoreColumns(gridRef.current.gridColumns, toRight, newZoomLevel, newX);
                updateGrid(newGridDefs);
            }
        },
        [updateGrid],
    );

    const moveViewPoint = useCallback(
        (level: ZoomLevel, zoomType: ZoomType): number | null => {
            if (
                (zoomLevelRef.current === ZoomLevel.DAILY && zoomType === ZoomType.IN) ||
                (zoomLevelRef.current === ZoomLevel.YEARLY && zoomType === ZoomType.OUT)
            ) {
                return null;
            }
            const zoomLevelSettings = ZOOM_LEVELS[level];
            const currentZoomSettings = ZOOM_LEVELS[zoomLevelRef.current];

            const currentX = mainStageRef.current?.x() || 0;
            const screenCenter = currentX - center;

            const currentDayWidth = currentZoomSettings.COLUMN_WIDTH / currentZoomSettings.DAYS_PER_COLUMN;
            const dayWidth = zoomLevelSettings.COLUMN_WIDTH / zoomLevelSettings.DAYS_PER_COLUMN;
            const ratio = currentDayWidth / dayWidth;
            const newX = screenCenter / ratio + center;

            namesLayerRef.current?.to({ x: -newX, duration: ANIMATION_DURATION.ZOOM });
            mainStageRef.current?.to({ x: newX, duration: ANIMATION_DURATION.ZOOM });
            headerStageRef.current?.to({ x: newX, duration: ANIMATION_DURATION.ZOOM });
            gridStageRef.current?.to({ x: newX, duration: ANIMATION_DURATION.ZOOM });

            return -newX;
        },
        [center],
    );

    const onBackToToday = useCallback(() => {
        namesLayerRef.current?.to({ x: -center, duration: ANIMATION_DURATION.ZOOM });
        mainStageRef.current?.to({ x: center, duration: ANIMATION_DURATION.ZOOM });
        headerStageRef.current?.to({ x: center, duration: ANIMATION_DURATION.ZOOM });
        gridStageRef.current?.to({ x: center, duration: ANIMATION_DURATION.ZOOM });
    }, [center]);

    const handleZoomChange = useCallback(
        (newLevel: ZoomLevel) => {
            const currentZoomLevel = zoomLevelRef.current;
            const zoomType = newLevel > currentZoomLevel ? ZoomType.OUT : ZoomType.IN;

            resizeNoodles(poolsRef.current, newLevel);
            const newX = moveViewPoint(newLevel, zoomType);
            gridRef.current = resizeGrid(gridRef.current.gridColumns, newLevel);
            if (newX !== null) {
                renderMoreColumnsOnZoomChange(newX, newLevel);

                // propagates event to pools
                poolsRef.current.forEach((pool) => {
                    pool.impl?.onScroll(newX);
                });
            }
            moveTodayMarker(todayMarkerRef.current, newLevel);
            zoomLevelRef.current = newLevel;

            if (currentZoomLevel !== newLevel) {
                const { startDate, endDate } = gridRef.current;
                setLimitDates({ startDate, endDate });
                loadMore(gridRef.current.startDate, gridRef.current.endDate);
            }
        },
        [loadMore, moveViewPoint, renderMoreColumnsOnZoomChange, setLimitDates],
    );

    useEffect(() => {
        handleZoomChange(zoomLevel);
    }, [handleZoomChange, zoomLevel]);

    useEffect(() => {
        drawGrid();
        drawHeader();
        batchDraw();
    }, [batchDraw, drawGrid, drawHeader]);

    const reactToNewData = useCallback(
        (newData: DataPool[]) => {
            updatePools(
                newData,
                mappedPoolsRef.current,
                mappedDividersRef.current,
                mappedNoodlesRef.current,
                poolsRef.current,
                dividersRef.current,
                zoomLevelRef.current,
                onUpdateLaneIndex,
                isExpandedMode,
            );
        },
        [isExpandedMode, onUpdateLaneIndex],
    );

    useEffect(() => {
        reactToNewData(data);

        drawPoolDividers();
        drawPools();
        drawNames();
        batchDraw();
    }, [batchDraw, data, drawNames, drawPoolDividers, drawPools, reactToNewData]);

    const toggleAll = useCallback(
        (isExpanded: boolean) => {
            toggleAllNoodles(poolsRef.current, dividersRef.current, isExpanded);
            batchDraw();
        },
        [batchDraw],
    );

    useEffect(() => toggleAll(isExpandedMode), [isExpandedMode, toggleAll]);

    return {
        stages: {
            main: mainStageRef,
            poolDividers: poolDividersStageRef,
            header: headerStageRef,
            grid: gridStageRef,
        },
        events: {
            onScroll,
            onBackToToday,
            recreateAllPools,
        },
    };
};

export default useTimeline;
