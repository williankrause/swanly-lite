interface BaseDataItem {
    id: string;
    text: string;
    color?: string;
    fontColor?: string;
    laneIndex?: number;
    icon?: string;
}

export interface DataItem extends BaseDataItem {
    start?: Date;
    end?: Date;
}

export interface ExpandableDataItem extends BaseDataItem {
    start?: Date;
    end?: Date;
    drawStart: Date;
    drawEnd: Date;
    subItems?: DataItem[];
}

export interface DataPool {
    id: string;
    text: string;
    order?: number;
    icon?: string;
    items: ExpandableDataItem[];
}

export enum ZoomLevel {
    DAILY = 0,
    WEEKLY = 1,
    MONTHLY = 2,
    QUARTERLY = 3,
    YEARLY = 4,
}

export enum ZoomType {
    IN,
    OUT,
}

export type ScrollCoords = { deltaX: number; deltaY: number };
export type OnScrollFunction = (coords: ScrollCoords) => void;
export type OnZoomChangeFunction = (newLevel: ZoomLevel) => void;
export type LoadMoreFunction = (start: Date, end: Date) => void;
export type UpdateLaneIndexFunction = (item: DataItem | ExpandableDataItem, newIndex: number) => void;
export type OnMoveToTodayFunction = () => void;
export type RecreatePoolsFunction = () => void;
export type SetOnMoveToTodayFunction = (onMoveToToday: OnMoveToTodayFunction) => void;
export type SetRecreatePoolsFunction = (recreatePools: RecreatePoolsFunction) => void;
export type OnToggleAllFunction = () => void;
