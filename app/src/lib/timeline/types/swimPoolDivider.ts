import SwimPoolDividerLineImpl from '../api/SwimPool/SwimPoolDividerLine';
import SwimPoolDividerNameImpl from '../api/SwimPool/SwimPoolDividerName';

export default interface SwimPoolDivider {
    id: string;
    poolId: string;
    y?: number;
    height: number;
    text: string;
    icon?: string;
    order?: number;
    nameImpl?: SwimPoolDividerNameImpl;
    lineImpl?: SwimPoolDividerLineImpl;
}
