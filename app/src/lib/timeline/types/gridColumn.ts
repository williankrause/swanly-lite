import GridColumnImpl from '../api/Grid/GridColumn';
import GridHeaderImpl from '../api/Grid/GridHeader';

export interface GridColumn {
    id: string;
    date: Date;
    header: {
        firstLine: string;
        secondLine: string;
    };
    x: number;
    width: number;
    columnImpl?: GridColumnImpl;
    headerImpl?: GridHeaderImpl;
}
