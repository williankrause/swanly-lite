import TodayMarkerImpl from '../api/Grid/TodayMarker';
import TodayLineImpl from '../api/Grid/TodayLine';

export interface TodayMarker {
    id: string;
    x: number;
    markerImpl?: TodayMarkerImpl;
    lineImpl?: TodayLineImpl;
}
