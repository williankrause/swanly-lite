import {
    addDays,
    addMonths,
    addQuarters,
    addWeeks,
    addYears,
    differenceInDays,
    endOfQuarter,
    endOfWeek,
    endOfYear,
    format,
    getDate,
    isSameDay,
    lastDayOfMonth,
    startOfDay,
    startOfMonth,
    startOfQuarter,
    startOfWeek,
    startOfYear,
    subDays,
    subMonths,
    subQuarters,
    subWeeks,
    subYears,
} from 'date-fns';
import ms from 'ms';
import { ZoomLevel } from './types/base';

export const SPEED = 500; // Pixels per second

export const NOODLE_HEIGHT = 24;
export const NOODLE_RADIUS = 3;

export const NOODLE_SHADOW_OFFSET = 1;
export const NOODLE_HOVERED_SHADOW_OFFSET = 2;
export const NOODLE_HOVERED_SHADOW_BLUR = 3;
export const NOODLE_SHADOW_BLUR = 2;
export const NOODLE_SHADOW_COLOR_TRANSPARENT_LEVEL = 0.84;

export const NOODLE_FONT_STYLE = 'normal';
export const NOODLE_FONT_SIZE = 12;
export const NOODLE_TEXT_PADDING = 7;

export const SPACER_HEIGHT = 8;

export const SWIMLANE_PADDING = 2;
export const SWIMLANE_HEIGHT = NOODLE_HEIGHT + SWIMLANE_PADDING * 2;
export const SWIMLANE_UNIQUE_ID_PREFIX = 'lane_';

export const SWIMLANE_Y_OFFSET = SWIMLANE_HEIGHT + SPACER_HEIGHT;

export const SWIMPOOL_AVATAR_SIZE = 24;
export const SWIMPOOL_AVATAR_PADDING = 9;

export const SWIMPOOL_DIVIDER_LINE_HEIGHT = 1;
export const SWIMPOOL_DIVIDER_LINE_PADDING = 11;
export const SWIMPOOL_DIVIDER_HEIGHT = NOODLE_HEIGHT + SWIMPOOL_DIVIDER_LINE_PADDING + SWIMPOOL_DIVIDER_LINE_HEIGHT;
export const SWIMPOOL_DIVIDER_NAME_RADIUS = 3;

export const SWIMPOOL_NAME_HEIGHT = NOODLE_HEIGHT / 2 + SWIMLANE_PADDING;
export const SWIMPOOL_NAME_HORIZONTAL_PADDING = SWIMPOOL_AVATAR_SIZE + SWIMPOOL_AVATAR_PADDING + 5;

export const GRID_COLUMNS_TO_LOAD = 40;
export const GRID_COLUMN_UNIQUE_ID_PREFIX = 'grid_column_';
export const GRID_COLUMN_BACKGROUND_IMAGE = './weekend-lines.png';

export const GRID_LINE_WIDTH = 1;
export const GRID_TEXT_PADDING = 8;
export const GRID_FONT_SIZE = 12;
export const GRID_TEXT_SPACING = 20;
export const GRID_SHADOW_COLOR_TRANSPARENT_LEVEL = 0.84;
export const GRID_SHADOW_BLUR = 5;
export const GRID_SHADOW_OFFSET_Y = 1;
export const GRID_SHADOW_OFFSET_X = 5;

export const ANIMATION_DURATION = {
    ZOOM: 0.8,
    EXPAND: 0.4,
};

export const STAGE_PADDING = 10;

export const BATHTUB_PADDING = 6;

export const HEADER_HEIGHT = 48;
export const HEADER_STAGE_HEIGHT = 56;
export const TIMELINE_HEADER_SPACING = HEADER_HEIGHT + SPACER_HEIGHT + SWIMLANE_PADDING;

export const RUBBER_DUCK_LINE_HEIGHT = 8;
export const RUBBER_DUCK_FONT_SIZE = 12;

export const STATUS_ICON_SIZE = 16;

export const ICON_OFFSET = STATUS_ICON_SIZE + NOODLE_TEXT_PADDING;

export const TODAY_MARKER_RADIUS = 8;
export const TODAY_MARKER_OUTER_OPACITY = 0.5;

export const DEFAULT_ZOOM = ZoomLevel.DAILY;

export const DEFAULT_TIMEOUT = ms('0.8s');
export const CSS_TRANSITION_CONFIG = 'all 0.1s ease-in-out';
export const CUBIC_TRANSITION_CONFIG = 'all 0.5s cubic-bezier(0.895, 0.03, 0.685, 0.22)';

type Header = {
    firstLine: string;
    secondLine: string;
};

export const ZOOM_LEVELS = {
    [ZoomLevel.DAILY]: {
        COLUMN_WIDTH: 100,
        DAY_WIDTH: (): number => 100,
        DAYS_PER_COLUMN: 1,
        DATE_FUNCTIONS: {
            add: addDays,
            sub: subDays,
            base: startOfDay,
            headerFormat: (date: Date): Header => {
                const firstLine = format(date, 'EEEE');
                const secondLine = format(date, 'MMM d');

                return {
                    firstLine,
                    secondLine,
                };
            },
        },
    },
    [ZoomLevel.WEEKLY]: {
        COLUMN_WIDTH: 200,
        DAY_WIDTH: (): number => 200 / 7,
        DAYS_PER_COLUMN: 7,
        DATE_FUNCTIONS: {
            add: addWeeks,
            sub: subWeeks,
            base: startOfWeek,
            headerFormat: (date: Date): Header => {
                const firstLine = `Week ${format(date, 'w')}`;
                const secondLine = `${format(date, 'MMM d')} - ${format(endOfWeek(date), 'MMM d')}`;

                return {
                    firstLine,
                    secondLine,
                };
            },
        },
    },
    [ZoomLevel.MONTHLY]: {
        COLUMN_WIDTH: 200,
        DAY_WIDTH: (date: Date): number => 200 / getDate(lastDayOfMonth(date)),
        DAYS_PER_COLUMN: 30,
        DATE_FUNCTIONS: {
            add: addMonths,
            sub: subMonths,
            base: startOfMonth,
            headerFormat: (date: Date): Header => {
                const firstLine = isSameDay(startOfYear(date), date) ? format(date, 'yyyy') : '';
                const secondLine = `${format(date, 'MMMM')}`;

                return {
                    firstLine,
                    secondLine,
                };
            },
        },
    },
    [ZoomLevel.QUARTERLY]: {
        COLUMN_WIDTH: 200,
        DAY_WIDTH: (date: Date): number => 200 / differenceInDays(endOfQuarter(date), startOfQuarter(date)),
        DAYS_PER_COLUMN: 90,
        DATE_FUNCTIONS: {
            add: addQuarters,
            sub: subQuarters,
            base: startOfQuarter,
            headerFormat: (date: Date): Header => {
                const firstLine = `${format(date, 'QQQ')}'${format(date, 'yy')}`;
                const secondLine = `${format(date, 'MMM')} - ${format(endOfQuarter(date), 'MMM')}`;

                return {
                    firstLine,
                    secondLine,
                };
            },
        },
    },
    [ZoomLevel.YEARLY]: {
        COLUMN_WIDTH: 300,
        DAY_WIDTH: (date: Date): number => 300 / differenceInDays(endOfYear(date), startOfYear(date)),
        DAYS_PER_COLUMN: 365,
        DATE_FUNCTIONS: {
            add: addYears,
            sub: subYears,
            base: startOfYear,
            headerFormat: (date: Date): Header => {
                const firstLine = '';
                const secondLine = format(date, 'yyyy');

                return {
                    firstLine,
                    secondLine,
                };
            },
        },
    },
};
