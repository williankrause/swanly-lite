import { CSSProperties, FunctionComponent, useCallback, useEffect, useState } from 'react';
import { KonvaEventObject } from 'konva/lib/Node';
import { ItemToggleFunction, NoodleNameClickFunction } from 'lib/timeline/api/SwimPool/PoolNoodle';
import { HEADER_STAGE_HEIGHT } from 'lib/timeline/constants';
import useTimeline from 'lib/timeline/hooks/useTimeline';
import { DataPool, LoadMoreFunction, UpdateLaneIndexFunction, ZoomLevel } from 'lib/timeline/types/base';
import { noop } from 'lodash';
import { Stage } from 'react-konva';
import { useTimelineEvents } from 'lib/timeline/contexts/timeline';

export type TimelineProps = {
    data: DataPool[];
    zoomLevel?: ZoomLevel;
    isExpandedMode?: boolean;
    heightOffset?: number;
    loadMore: LoadMoreFunction;
    onItemToggle?: ItemToggleFunction;
    onItemNameClick?: NoodleNameClickFunction;
    onUpdateLaneIndex?: UpdateLaneIndexFunction;
    isDarkMode?: boolean;
};

type ExtendedTimelineProps = TimelineProps & {
    zoomLevel: ZoomLevel;
    isExpandedMode: boolean;
    center: number;
    width: number;
    height: number;
    backgroundImage: HTMLImageElement | undefined;
};

const stageStyle: CSSProperties = { position: 'absolute', top: 48, left: 0 };

const Timeline: FunctionComponent<ExtendedTimelineProps> = ({
    data,
    zoomLevel,
    isExpandedMode,
    center,
    width,
    height,
    backgroundImage,
    loadMore,
    onItemToggle = noop,
    onItemNameClick = noop,
    onUpdateLaneIndex = noop,
    isDarkMode = false,
}) => {
    const [isDragging, setIsDragging] = useState<boolean>(false);

    const { setOnMoveToToday, setRecreatePools, setLimitDates } = useTimelineEvents();

    const { stages, events } = useTimeline(
        zoomLevel,
        loadMore,
        data,
        width,
        height,
        center,
        backgroundImage,
        onItemToggle,
        onItemNameClick,
        onUpdateLaneIndex,
        isExpandedMode,
        setLimitDates,
        isDarkMode,
    );

    useEffect(() => {
        setOnMoveToToday(events.onBackToToday);
        setRecreatePools(events.recreateAllPools);
    }, [events.onBackToToday, events.recreateAllPools, setOnMoveToToday, setRecreatePools]);

    const onMouseDown = useCallback(() => setIsDragging(true), []);
    const onMouseUp = useCallback(() => setIsDragging(false), []);
    const onMouseMove = useCallback(
        ({ evt }: KonvaEventObject<MouseEvent>) => {
            if (isDragging) {
                // Need to be inverted otherwise will scroll on wrong direction
                events.onScroll({ deltaX: -evt.movementX, deltaY: -evt.movementY });
            }
        },
        [events, isDragging],
    );
    const onWheel = useCallback(
        ({ evt }: KonvaEventObject<WheelEvent>) => {
            evt.preventDefault();
            events.onScroll({ deltaX: evt.deltaX, deltaY: evt.deltaY });
        },
        [events],
    );
    return (
        <>
            <Stage ref={stages.grid} x={center} width={width} height={height} style={stageStyle} />
            <Stage ref={stages.poolDividers} width={width} height={height} style={stageStyle} />
            <Stage
                ref={stages.main}
                x={center}
                width={width}
                height={height}
                onMouseDown={onMouseDown}
                onMouseMove={onMouseMove}
                onMouseUp={onMouseUp}
                onWheel={onWheel}
                style={stageStyle}
            />
            <Stage
                ref={stages.header}
                x={center}
                width={width}
                height={HEADER_STAGE_HEIGHT}
                style={stageStyle}
                onMouseDown={onMouseDown}
                onMouseMove={onMouseMove}
                onMouseUp={onMouseUp}
                onWheel={onWheel}
            />
        </>
    );
};

export default Timeline;
