import { transparentize } from "polished";
import { GRID_SHADOW_COLOR_TRANSPARENT_LEVEL, NOODLE_SHADOW_COLOR_TRANSPARENT_LEVEL } from "../constants";

export const NOODLE_SHADOW_COLOR = '#000000';
export const NOODLE_FONT_COLOR = '#172B4D';
export const NOODLE_FONT_COLOR_DARK = '#172b4d';
export const NOODLE_DEFAULT_COLOR = '#3366ff';
export const SWIMPOOL_DIVIDER_COLOR = '#c1c7d0';
export const GRID_LINE_COLOR = '#ebecf0';
export const GRID_TEXT_COLOR_FIRST_LINE = '#6b778c';
export const GRID_TEXT_COLOR_SECOND_LINE = '#172b4d';
export const GRID_FILL_COLOR = '#ffffff';
export const GRID_SHADOW_COLOR = '#000000';
export const BATHTUB_BACKGROUND_COLOR = '#dfe1e6';
export const TODAY_MARKER_COLOR = '#6554c0';
export const NOODLE_TRANSPARENT_COLOR = transparentize(NOODLE_SHADOW_COLOR_TRANSPARENT_LEVEL, NOODLE_SHADOW_COLOR);
export const GRID_TRANSPARENT_COLOR = transparentize(GRID_SHADOW_COLOR_TRANSPARENT_LEVEL, NOODLE_SHADOW_COLOR);
export const GRADIENT_STOP = '#FAFBFC';
export const TAG_FILL = '#FFFFFF';
