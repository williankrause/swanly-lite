import { FunctionComponent, useEffect, useMemo, useRef } from 'react';
import styled from '@emotion/styled';
import { useSetState } from 'react-use';
import useMeasure from 'use-measure';
import useImage from 'use-image';
import Timeline, { TimelineProps } from './components/Timeline/Timeline';
import { CSS_TRANSITION_CONFIG, DEFAULT_ZOOM, GRID_COLUMN_BACKGROUND_IMAGE } from './constants';
import * as lightTheme from './theme/light';
import * as darkTheme from './theme/dark';

type DimensionsState = {
    width: number;
    height: number;
};

const Container = styled.div<{ backgroundColor: string }>`
    flex-grow: 1;
    width: 100%;
    height: 800px;
    background-color: ${({ backgroundColor }) => backgroundColor};
    transition: ${CSS_TRANSITION_CONFIG};
`;

const TimelineContainer: FunctionComponent<TimelineProps> = ({
    data,
    zoomLevel = DEFAULT_ZOOM,
    isExpandedMode = false,
    heightOffset = 0,
    loadMore,
    onItemToggle,
    onItemNameClick,
    onUpdateLaneIndex,
    isDarkMode,
}) => {
    const [backgroundImage] = useImage(GRID_COLUMN_BACKGROUND_IMAGE);

    const backgroundColor = useMemo(() => isDarkMode ? darkTheme.GRID_FILL_COLOR : lightTheme.GRID_FILL_COLOR, [isDarkMode])

    const divRef = useRef<HTMLDivElement | null>(null);

    const { width: divWidth, height: divHeight } = useMeasure(divRef);
    const [{ width, height }, setDimensions] = useSetState<DimensionsState>({ width: divWidth, height: divHeight });
    useEffect(() => {
        setDimensions({ width: divWidth, height: divHeight - heightOffset });
        console.log('dinemsions changed', divWidth, divHeight)
    }, [divHeight, divWidth, heightOffset, setDimensions]);

    const center = useMemo(() => width / 2, [width]);

    return (
        <Container ref={divRef} backgroundColor={backgroundColor}>
            {!!center && !!width && !!height && backgroundImage && (
                <Timeline
                    data={data}
                    zoomLevel={zoomLevel}
                    isExpandedMode={isExpandedMode}
                    center={center}
                    width={width}
                    height={height}
                    backgroundImage={backgroundImage}
                    loadMore={loadMore}
                    onItemToggle={onItemToggle}
                    onItemNameClick={onItemNameClick}
                    onUpdateLaneIndex={onUpdateLaneIndex}
                    isDarkMode={isDarkMode}
                />
            )}
        </Container>
    );
};

export default TimelineContainer;
