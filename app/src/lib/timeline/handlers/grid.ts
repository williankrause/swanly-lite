/* eslint-disable no-param-reassign */
import { differenceInDays, startOfToday } from 'date-fns';
import { first, last, range, uniqueId } from 'lodash';
import { GRID_COLUMNS_TO_LOAD, GRID_COLUMN_UNIQUE_ID_PREFIX, ZOOM_LEVELS } from '../constants';
import { ZoomLevel } from '../types/base';
import { GridColumn } from '../types/gridColumn';
import { TodayMarker } from '../types/todayMarker';

export type GridSettings = {
    gridColumns: GridColumn[];
    startDate: Date;
    endDate: Date;
    leftThreshold: number;
    rightThreshold: number;
};

const calculateMarkerX = (zoomLevel: ZoomLevel) => {
    const zoomLevelSettings = ZOOM_LEVELS[zoomLevel];

    const today = startOfToday();

    const baseDate = zoomLevelSettings.DATE_FUNCTIONS.base(today);

    const difference = Math.abs(differenceInDays(today, baseDate));

    const dayWidth = zoomLevelSettings.DAY_WIDTH(today);

    return difference * dayWidth + dayWidth / 2;
};

export const buildTodayMarker = (zoomLevel: ZoomLevel): TodayMarker => {
    return {
        id: 'today_marker',
        x: calculateMarkerX(zoomLevel),
    };
};

export const moveTodayMarker = (marker: TodayMarker, zoomLevel: ZoomLevel): TodayMarker => {
    marker.x = calculateMarkerX(zoomLevel);
    marker.lineImpl?.onTransition({ x: marker.x });
    marker.markerImpl?.onTransition({ x: marker.x });

    return marker;
};

const twoThirds = (value: number) => (value / 3) * 2;

export const buildGrid = (zoomLevel: ZoomLevel): GridSettings => {
    const zoomLevelSettings = ZOOM_LEVELS[zoomLevel];
    const baseDate = zoomLevelSettings.DATE_FUNCTIONS.base(startOfToday());
    let gridColumns: GridColumn[] = [
        {
            id: uniqueId(GRID_COLUMN_UNIQUE_ID_PREFIX),
            date: baseDate,
            header: zoomLevelSettings.DATE_FUNCTIONS.headerFormat(baseDate),
            x: 0,
            width: zoomLevelSettings.COLUMN_WIDTH,
        },
    ];

    for (let i = 1; i <= GRID_COLUMNS_TO_LOAD; i += 1) {
        const xOffset = zoomLevelSettings.COLUMN_WIDTH * i;
        const pastDate = zoomLevelSettings.DATE_FUNCTIONS.sub(baseDate, i);
        const futureDate = zoomLevelSettings.DATE_FUNCTIONS.add(baseDate, i);
        gridColumns = [
            {
                id: uniqueId(GRID_COLUMN_UNIQUE_ID_PREFIX),
                date: pastDate,
                header: zoomLevelSettings.DATE_FUNCTIONS.headerFormat(pastDate),
                x: -xOffset,
                width: zoomLevelSettings.COLUMN_WIDTH,
            },
            ...gridColumns,
            {
                id: uniqueId(GRID_COLUMN_UNIQUE_ID_PREFIX),
                date: futureDate,
                header: zoomLevelSettings.DATE_FUNCTIONS.headerFormat(futureDate),
                x: xOffset,
                width: zoomLevelSettings.COLUMN_WIDTH,
            },
        ];
    }

    const firstColumn = first(gridColumns);
    const lastColumn = last(gridColumns);

    const leftThreshold = twoThirds(firstColumn?.x || 0);
    const rightThreshold = twoThirds(lastColumn?.x || 0);

    return {
        gridColumns,
        leftThreshold,
        rightThreshold,
        startDate: firstColumn?.date || new Date(),
        endDate: lastColumn?.date || new Date(),
    };
};

export const renderMoreColumns = (
    actualColumns: GridColumn[],
    toRight: boolean,
    zoomLevel: ZoomLevel,
    untilX?: number,
): GridSettings => {
    const zoomLevelSettings = ZOOM_LEVELS[zoomLevel];
    const column = toRight ? last(actualColumns) : first(actualColumns);
    const baseDate = column?.date || 0;
    const latestX = column?.x || 0;

    let gridColumns: GridColumn[] = [];

    let columnsToRender = GRID_COLUMNS_TO_LOAD;
    if (untilX) {
        if (latestX > 0 && untilX > latestX) {
            columnsToRender += Math.ceil((untilX - latestX) / zoomLevelSettings.COLUMN_WIDTH);
        } else if (latestX < 0 && latestX > untilX) {
            columnsToRender += Math.floor((latestX - untilX) / zoomLevelSettings.COLUMN_WIDTH);
        }
    }

    range(1, columnsToRender + 1).forEach((amount) => {
        const xOffset = amount * zoomLevelSettings.COLUMN_WIDTH;
        const newDate = toRight
            ? zoomLevelSettings.DATE_FUNCTIONS.add(baseDate, amount)
            : zoomLevelSettings.DATE_FUNCTIONS.sub(baseDate, amount);

        const newColumn = {
            id: uniqueId(GRID_COLUMN_UNIQUE_ID_PREFIX),
            date: newDate,
            header: zoomLevelSettings.DATE_FUNCTIONS.headerFormat(newDate),
            x: toRight ? latestX + xOffset : latestX - xOffset,
            width: zoomLevelSettings.COLUMN_WIDTH,
        };

        if (toRight) {
            gridColumns = [...gridColumns, newColumn];
        } else {
            gridColumns = [newColumn, ...gridColumns];
        }
    });

    const startDate = first(gridColumns)?.date || new Date();
    const endDate = last(gridColumns)?.date || new Date();

    gridColumns = toRight ? [...actualColumns, ...gridColumns] : [...gridColumns, ...actualColumns];

    const leftThreshold = twoThirds(first(gridColumns)?.x || 0);
    const rightThreshold = twoThirds(last(gridColumns)?.x || 0);

    return {
        gridColumns,
        leftThreshold,
        rightThreshold,
        startDate,
        endDate,
    };
};

export const resizeGrid = (actualColumns: GridColumn[], zoomLevel: ZoomLevel): GridSettings => {
    const zoomLevelSettings = ZOOM_LEVELS[zoomLevel];
    const baseDate = zoomLevelSettings.DATE_FUNCTIONS.base(startOfToday());

    const [past, future] = actualColumns.reduce<Array<GridColumn[]>>(
        ([left, right], column) => {
            if (column.x === 0) {
                return [
                    [column, ...left],
                    [column, ...right],
                ];
            }
            if (column.x < 0) {
                return [[column, ...left], right];
            }
            return [left, [...right, column]];
        },
        [[], []],
    );

    const iterateUntil = Math.max(past.length, future.length);

    for (let i = 0; i < iterateUntil; i += 1) {
        if (i === 0) {
            const center = past[i];
            const difference = zoomLevelSettings.COLUMN_WIDTH - center.width;
            center.width += difference;
            center.date = baseDate;
            center.header = zoomLevelSettings.DATE_FUNCTIONS.headerFormat(baseDate);
            center.headerImpl?.onTransition({ width: center.width, newHeader: center.header });
            center.columnImpl?.onZoomChange(zoomLevel, center.date, { width: center.width });

            // eslint-disable-next-line no-continue
            continue;
        }

        const pastColumn = past[i];
        const futureColumn = future[i];

        if (pastColumn) {
            const pastDate = zoomLevelSettings.DATE_FUNCTIONS.sub(baseDate, i);
            const difference = zoomLevelSettings.COLUMN_WIDTH - pastColumn.width;
            pastColumn.width += difference;
            pastColumn.x -= difference * i;
            pastColumn.date = pastDate;
            pastColumn.header = zoomLevelSettings.DATE_FUNCTIONS.headerFormat(pastDate);
            pastColumn.headerImpl?.onTransition({
                width: pastColumn.width,
                x: pastColumn.x,
                newHeader: pastColumn.header,
            });
            pastColumn.columnImpl?.onZoomChange(zoomLevel, pastColumn.date, {
                width: pastColumn.width,
                x: pastColumn.x,
            });
        }
        if (futureColumn) {
            const futureDate = zoomLevelSettings.DATE_FUNCTIONS.add(baseDate, i);
            const difference = zoomLevelSettings.COLUMN_WIDTH - futureColumn.width;
            futureColumn.width += difference;
            futureColumn.x += difference * i;
            futureColumn.date = futureDate;
            futureColumn.header = zoomLevelSettings.DATE_FUNCTIONS.headerFormat(futureDate);
            futureColumn.headerImpl?.onTransition({
                width: futureColumn.width,
                x: futureColumn.x,
                newHeader: futureColumn.header,
            });
            futureColumn.columnImpl?.onZoomChange(zoomLevel, futureColumn.date, {
                width: futureColumn.width,
                x: futureColumn.x,
            });
        }
    }

    const gridColumns = [...past.reverse(), ...future.slice(1)];

    const firstColumn = first(gridColumns);
    const lastColumn = last(gridColumns);

    const startDate = firstColumn?.date || new Date();
    const endDate = lastColumn?.date || new Date();
    const leftThreshold = twoThirds(firstColumn?.x || 0);
    const rightThreshold = twoThirds(lastColumn?.x || 0);

    return {
        gridColumns,
        leftThreshold,
        rightThreshold,
        startDate,
        endDate,
    };
};
