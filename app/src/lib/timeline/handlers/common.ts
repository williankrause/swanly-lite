import { SPEED } from '../constants';

// Calculates transition duration based on speed
export const calculateDuration = (distance: number): number => Math.abs(distance / SPEED);
