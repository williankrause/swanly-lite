/* eslint-disable no-param-reassign */
import { eachDayOfInterval, isBefore, startOfToday } from 'date-fns';
import { uniqueId, maxBy, flatten, sortBy } from 'lodash';
import { BathtubLane } from 'lib/timeline/api/Bathtub/BathtubLane';
import { PoolNoodle } from 'lib/timeline/api/SwimPool/PoolNoodle';
import { SwimPool } from 'lib/timeline/api/SwimPool/SwimPool';
import {
    ZOOM_LEVELS,
    SWIMLANE_UNIQUE_ID_PREFIX,
    SWIMLANE_Y_OFFSET,
    NOODLE_HEIGHT,
    SWIMLANE_HEIGHT,
    SPACER_HEIGHT,
    SWIMLANE_PADDING,
} from 'lib/timeline/constants';
import { DataItem, DataPool, ExpandableDataItem, UpdateLaneIndexFunction, ZoomLevel } from 'lib/timeline/types/base';
import SwimPoolDivider from 'lib/timeline/types/swimPoolDivider';
import { ExpandedNoodle, SwimLane } from '../api/SwimPool/SwimLane';

export type MappedSwimPools = {
    [key: string]: SwimPool;
};

export type MappedSwimPoolDividers = {
    [key: string]: SwimPoolDivider;
};

export type MappedPoolNoodles = {
    [key: string]: PoolNoodle;
};

type ComparisionItem = { start: number; end: number };

const isOverlapping = (existingItem: ComparisionItem, itemBeingAdded: ComparisionItem) =>
    (existingItem.start >= itemBeingAdded.start && existingItem.start < itemBeingAdded.end) ||
    (existingItem.end > itemBeingAdded.start && existingItem.end < itemBeingAdded.end) ||
    (itemBeingAdded.start > existingItem.start && itemBeingAdded.start < existingItem.end) ||
    (itemBeingAdded.end > existingItem.start && itemBeingAdded.end < existingItem.end);

const getXAndWidth = (
    startDate: Date,
    endDate: Date,
    baseDate: Date,
    zoomLevel: ZoomLevel,
): { x: number; width: number } => {
    const zoomLevelSettings = ZOOM_LEVELS[zoomLevel];

    const daysForWidth = eachDayOfInterval({
        start: startDate,
        end: isBefore(endDate, startDate) ? startDate : endDate,
    });

    const width = daysForWidth.reduce((prev, date) => prev + zoomLevelSettings.DAY_WIDTH(date), 0);

    const dateInterval = isBefore(startDate, baseDate)
        ? { start: startDate, end: baseDate }
        : { start: baseDate, end: startDate };
    const daysForX = eachDayOfInterval(dateInterval);

    // Last day in array must not be considered in calculations,
    // that is compensated by width
    daysForX.pop();

    const multiplier = isBefore(startDate, baseDate) ? -1 : 1;

    const x = daysForX.reduce((prev, date) => prev + zoomLevelSettings.DAY_WIDTH(date), 0) * multiplier;

    return {
        x: x + 1,
        width: Math.max(width - 1, 1),
    };
};

const buildBathtub = (subItems: DataItem[] | undefined, baseDate: Date, zoomLevel: ZoomLevel): BathtubLane[] => {
    if (!subItems) {
        return [];
    }

    const lanes: BathtubLane[] = [];

    sortBy(subItems, 'id').forEach((subItem) => {
        if (!subItem.start && !subItem.end) {
            return;
        }

        const startDate = (subItem.start || subItem.end) as Date;
        const endDate = (subItem.end || subItem.start) as Date;
        const { x, width } = getXAndWidth(startDate, endDate, baseDate, zoomLevel);

        // Adjust x position to avoid overlapping with grid line
        const realX = x - 1;

        // If item has a pre-calculated laneIndex, we use it (will be used when Drag and Drop is implemented)
        let { laneIndex } = subItem;

        // If laneIndex is not defined, then we calculate the correct one
        if (laneIndex === undefined) {
            // If we can not find a suitable lane, we create a new one at the end of the array
            laneIndex = lanes.length;

            // `for` statement is used to allow breaking and avoiding unecessary processments
            for (let index = 0; index < lanes.length; index += 1) {
                // If item does not overlap with items in an existing lane, add it to that lane
                if (
                    lanes[index] &&
                    !lanes[index].ducks.find((existingDuck) =>
                        isOverlapping(
                            { start: existingDuck.x, end: existingDuck.x + existingDuck.width },
                            { start: realX, end: realX + width },
                        ),
                    )
                ) {
                    laneIndex = index;
                    break;
                }
            }

            const duck = {
                id: subItem.id,
                x: realX,
                width,
                text: subItem.text,
                color: subItem.color,
                icon: subItem.icon,
                item: subItem,
            };

            // If the lane already exists, just append the duck to it
            // Else, create a new lane at the end of the array
            if (lanes[laneIndex]) {
                lanes[laneIndex].ducks.push(duck);
            } else {
                const laneId = uniqueId(SWIMLANE_UNIQUE_ID_PREFIX);
                lanes[laneIndex] = {
                    id: laneId,
                    ducks: [duck],
                };
            }
        }
    });

    // The y coordinate for each lane is calculated using its index in the array multiplied by the
    // default SWIMLANE_HEIGHT plus a Spacer offset (will be used later on drag and drop)
    lanes.forEach((lane, index) => {
        lane.y = index * SWIMLANE_Y_OFFSET;
    });

    return lanes;
};

type BuildPoolType = {
    swimPool: SwimPool;
    divider: SwimPoolDivider;
};

const getNoodleCoordinates = (
    item: ExpandableDataItem,
    baseDate: Date,
    zoomLevel: ZoomLevel,
): { x: number; width: number; innerX?: number; innerWidth?: number } => {
    // Draw start and/or draw end should always come from API. If they don't come, there's something wrong.
    const drawStart = item.drawStart || item.drawEnd;
    const drawEnd = item.drawEnd || item.drawStart;

    // Coordinates related to stages start and end dates
    const { x, width } = getXAndWidth(drawStart, drawEnd, baseDate, zoomLevel);

    // Coordinates related to stages start and end dates
    let innerX;
    let innerWidth;
    if (item.start || item.end) {
        const startDate = item.start || item.end;
        const endDate = item.end || item.start;

        ({ x: innerX, width: innerWidth } = getXAndWidth(
            startDate as Date,
            endDate as Date,
            item.drawStart,
            zoomLevel,
        ));
    }

    return { x, width, innerX, innerWidth };
};

const buildNoodle = (
    item: ExpandableDataItem,
    poolId: string,
    baseDate: Date,
    zoomLevel: ZoomLevel,
    isExpanded: boolean,
): PoolNoodle => {
    const { x, width, innerX, innerWidth } = getNoodleCoordinates(item, baseDate, zoomLevel);

    const drawStart = item.drawStart || item.drawEnd;
    const bathtub = buildBathtub(item.subItems, drawStart, zoomLevel);

    let height = NOODLE_HEIGHT;
    if (isExpanded) {
        // We add the panel height to the existing noodle height (usually NOODLE_HEIGHT)
        height += bathtub.length * SWIMLANE_Y_OFFSET;
    }

    return {
        id: item.id,
        x,
        width,
        innerX,
        innerWidth,
        text: item.text,
        color: item.color,
        height,
        isExpanded,
        poolId,
        bathtub,
        item,
        icon: item.icon,
    };
};

const getLargestNoodleHeight = (expandedNoodles: ExpandedNoodle[]): number =>
    maxBy(expandedNoodles, ({ height }) => height)?.height || 0;

const calculatePoolHeight = (swimPool: SwimPool): number =>
    swimPool.lanes.reduce((acc, lane) => acc + lane.height + SPACER_HEIGHT, SWIMLANE_Y_OFFSET);

const updateNoodle = (
    item: ExpandableDataItem,
    poolId: string,
    noodle: PoolNoodle,
    baseDate: Date,
    zoomLevel: ZoomLevel,
    isExpandedMode: boolean,
    swimPool: SwimPool,
): void => {
    const { x, width, innerX, innerWidth } = getNoodleCoordinates(item, baseDate, zoomLevel);

    const bathtub = buildBathtub(item.subItems, item.drawStart, zoomLevel);

    let height = NOODLE_HEIGHT;
    if (noodle.isExpanded || isExpandedMode) {
        // We add the panel height to the existing noodle height (usually NOODLE_HEIGHT)
        height += bathtub.length * SWIMLANE_Y_OFFSET;

        const noodleLane = swimPool.lanes.find(({ id }) => id === noodle.laneId) as SwimLane;
        const expandedNoodle = noodleLane.expandedNoodles.find(({ id }) => noodle.id === id);
        if (expandedNoodle) {
            expandedNoodle.height = height;
            noodleLane.height = getLargestNoodleHeight(noodleLane.expandedNoodles);
            swimPool.height = calculatePoolHeight(swimPool);
        }
    }

    const iconChanged = item.icon !== noodle.icon;

    const laneIndexesToDelete: number[] = [];

    noodle.bathtub.forEach((lane, index) => {
        const newLane = bathtub[index];

        if (newLane) {
            const ducksToRemove: string[] = [];

            // Process existing ducks
            lane.ducks.forEach((duck) => {
                const newDuck = newLane.ducks.find(({ id }) => id === duck.id);
                // If duck exists on existing lane, we just update it
                if (newDuck) {
                    const duckIconChanged = duck.icon !== newDuck.icon;
                    Object.assign(duck, {
                        x: newDuck.x,
                        width: newDuck.width,
                        text: newDuck.text,
                        icon: newDuck.icon,
                        color: newDuck.color,
                        item: newDuck.item,
                    });
                    duck.impl?.onUpdateDuck(duckIconChanged, noodle.isExpanded);
                } else {
                    // Means the duck was either removed, or moved to another lane, in that case we remove it.
                    ducksToRemove.push(duck.id);
                }
            });

            // Removes ducks from lane
            lane.ducks = lane.ducks.filter((duck) => {
                const shouldRemove = ducksToRemove.includes(duck.id);
                if (shouldRemove) {
                    duck.impl?.group.destroyChildren();
                    duck.impl?.group.destroy();
                }
                return !shouldRemove;
            });

            // Check if lane has new ducks
            newLane.ducks.forEach((newDuck) => {
                const existingDuck = lane.ducks.some(({ id }) => id === newDuck.id);
                if (!existingDuck) {
                    // Means it's a new duck, let's add it
                    lane.ducks.push(newDuck);
                }
            });
        } else {
            // Means the lane does not exist on the new bathtub
            laneIndexesToDelete.push(index);
        }
    });

    bathtub.forEach((newLane, index) => {
        const existingLane = noodle.bathtub[index];
        if (!existingLane) {
            noodle.bathtub[index] = newLane;
        }
    });

    laneIndexesToDelete.forEach((index) => {
        const laneToDelete = noodle.bathtub[index];
        laneToDelete.impl?.group.destroy();

        delete noodle.bathtub[index];
    });

    Object.assign(noodle, {
        x,
        width,
        innerX,
        innerWidth,
        text: item.text,
        color: item.color,
        height,
        poolId,
        item,
        icon: item.icon,
    });

    noodle.impl?.onUpdateNoodle(iconChanged);
};

const calculateLaneHeight = (largestNoodleHeight: number) =>
    Math.max(largestNoodleHeight + SWIMLANE_PADDING + SPACER_HEIGHT, SWIMLANE_HEIGHT);

const calculateLanesY = (lanes: SwimLane[]) =>
    lanes.reduce((nextLaneY, lane) => {
        lane.y = nextLaneY;
        return nextLaneY + lane.height + SPACER_HEIGHT;
    }, SWIMLANE_Y_OFFSET);

const addToLane = (
    item: ExpandableDataItem,
    swimPool: SwimPool,
    noodle: PoolNoodle,
    indexUpdatedItems: ExpandableDataItem[],
) => {
    const { lanes } = swimPool;

    // If item has a pre-calculated laneIndex, we use it (will be used when Drag and Drop is implemented)
    let { laneIndex } = item;

    // If laneIndex is not defined, then we calculate the correct one
    if (laneIndex === undefined) {
        // If we can not find a suitable lane, we create a new one at the end of the array
        laneIndex = lanes.length;

        // `for` statement is used to allow breaking and avoiding unecessary processments
        for (let index = 0; index < lanes.length; index += 1) {
            // If item does not overlap with items in an existing lane, add it to that lane
            if (
                lanes[index] &&
                !lanes[index].noodles.find((existingNoodle) =>
                    isOverlapping(
                        { start: existingNoodle.x, end: existingNoodle.x + existingNoodle.width },
                        { start: noodle.x, end: noodle.x + noodle.width },
                    ),
                )
            ) {
                laneIndex = index;
                break;
            }
        }
        item.laneIndex = laneIndex;
        indexUpdatedItems.push(item);
    }

    // If the lane already exists, just append the noodle to it
    // Else, create a new lane at the end of the array
    const existingLane = lanes[laneIndex];
    if (existingLane) {
        noodle.laneId = existingLane.id;
        existingLane.noodles.push(noodle);

        if (noodle.isExpanded) {
            existingLane.expandedNoodles.push({ id: noodle.id, height: noodle.height });
            const largestNoodleHeight = getLargestNoodleHeight(existingLane.expandedNoodles);
            existingLane.height = calculateLaneHeight(largestNoodleHeight);
            swimPool.height = calculatePoolHeight(swimPool);
        }
    } else {
        const laneId = uniqueId(SWIMLANE_UNIQUE_ID_PREFIX);
        noodle.laneId = laneId;

        const newLane = {
            id: laneId,
            y: laneIndex * SWIMLANE_Y_OFFSET + SWIMLANE_Y_OFFSET,
            height: noodle.isExpanded ? calculateLaneHeight(noodle.height) : SWIMLANE_HEIGHT,
            noodles: [noodle],
            expandedNoodles: noodle.isExpanded ? [{ id: noodle.id, height: noodle.height }] : [],
        };

        lanes[laneIndex] = newLane;
        swimPool.height = calculatePoolHeight(swimPool);
    }
};

const buildPool = (
    dataPool: DataPool,
    startDate: Date,
    zoomLevel: ZoomLevel,
    indexUpdatedItems: ExpandableDataItem[],
    mappedNoodles: MappedPoolNoodles,
    isExpanded: boolean,
): BuildPoolType => {
    const swimPool: SwimPool = {
        id: dataPool.id,
        height: SWIMLANE_Y_OFFSET,
        lanes: [],
        order: dataPool.order,
    };

    dataPool.items.forEach((item) => {
        const noodle = buildNoodle(item, dataPool.id, startDate, zoomLevel, isExpanded);

        addToLane(item, swimPool, noodle, indexUpdatedItems);

        mappedNoodles[`${swimPool.id}-${noodle.id}`] = noodle;
    });

    calculateLanesY(swimPool.lanes);

    const divider = {
        id: `${dataPool.id}_divider`,
        poolId: dataPool.id,
        text: dataPool.text,
        icon: dataPool.icon,
        height: swimPool.height,
        order: dataPool.order,
    };

    return { swimPool, divider };
};

const checkForOverlap = (
    item: ExpandableDataItem,
    noodle: PoolNoodle,
    swimPool: SwimPool,
    indexUpdatedItems: ExpandableDataItem[],
) => {
    const laneIndex = swimPool.lanes.findIndex((lane) => lane.id === noodle.laneId);
    const noodleLane = swimPool.lanes[laneIndex as number];

    const hasOverlap = noodleLane.noodles.some(
        (laneNoodle) =>
            laneNoodle.id !== noodle.id &&
            isOverlapping(
                { start: laneNoodle.x, end: laneNoodle.x + laneNoodle.width },
                { start: noodle.x, end: noodle.x + noodle.width },
            ),
    );

    if (hasOverlap) {
        // Remove noodle from it's current lane
        noodleLane.noodles.splice(
            noodleLane.noodles.findIndex(({ id }) => id === noodle.id),
            1,
        );
        noodle.impl?.group.remove();
        delete noodle.impl;

        const nextLaneIndex = (laneIndex as number) + 1;
        let nextLane = swimPool.lanes[nextLaneIndex];

        if (!nextLane) {
            nextLane = {
                id: uniqueId(SWIMLANE_UNIQUE_ID_PREFIX),
                y: nextLaneIndex * SWIMLANE_Y_OFFSET + SWIMLANE_Y_OFFSET,
                height: SWIMLANE_HEIGHT,
                noodles: [],
                expandedNoodles: [],
            };
            swimPool.lanes[nextLaneIndex] = nextLane;
        }

        // Check if there's an overlap in next lane
        const nextLaneHasOverlap = nextLane.noodles.some(
            (laneNoodle) =>
                laneNoodle.id !== noodle.id &&
                isOverlapping(
                    { start: laneNoodle.x, end: laneNoodle.x + laneNoodle.width },
                    { start: noodle.x, end: noodle.x + noodle.width },
                ),
        );

        if (nextLaneHasOverlap) {
            // There is an overlap in next lane, let's add a new lane between current and next lanes
            const newLane = {
                id: uniqueId(SWIMLANE_UNIQUE_ID_PREFIX),
                y: nextLaneIndex * SWIMLANE_Y_OFFSET + SWIMLANE_Y_OFFSET,
                height: noodle.isExpanded ? calculateLaneHeight(noodle.height) : SWIMLANE_HEIGHT,
                noodles: [noodle],
                expandedNoodles: noodle.isExpanded ? [{ id: noodle.id, height: noodle.height }] : [],
            };

            swimPool.lanes.splice(nextLaneIndex, 0, newLane);
            calculateLanesY(swimPool.lanes);

            swimPool.lanes.forEach((lane, index) => {
                lane.noodles.forEach((laneNoodle) => {
                    laneNoodle.item.laneIndex = index;
                    laneNoodle.laneId = lane.id;
                    indexUpdatedItems.push(laneNoodle.item);
                });
                lane.impl?.onTransition({ y: lane.y });
            });
        } else {
            // No overlap in next lane, let's move the noodle

            // Update lane index in item and noodle
            item.laneIndex = nextLaneIndex;
            noodle.laneId = nextLane.id;

            // Add noodle to new lane
            nextLane.noodles.push(noodle);

            // Calculate new heights if noodle is expanded
            if (noodle.isExpanded) {
                // Calculate new height of new lane
                nextLane.expandedNoodles.push({ id: noodle.id, height: noodle.height });
                const nextLaneLargestNoodleHeight = getLargestNoodleHeight(nextLane.expandedNoodles);
                nextLane.height = calculateLaneHeight(nextLaneLargestNoodleHeight);

                // Calculate new height of old lane
                noodleLane.expandedNoodles.splice(
                    noodleLane.expandedNoodles.findIndex(({ id }) => id === noodle.id),
                    1,
                );
                const previousLaneLargestNoodleHeight = getLargestNoodleHeight(noodleLane.expandedNoodles);
                noodleLane.height = calculateLaneHeight(previousLaneLargestNoodleHeight);
            }
        }
        // Calculate new size of pool
        swimPool.height = calculatePoolHeight(swimPool);
        indexUpdatedItems.push(item);
    }
};

export const updatePools = (
    dataPools: DataPool[],
    mappedPools: MappedSwimPools,
    mappedDividers: MappedSwimPoolDividers,
    mappedNoodles: MappedPoolNoodles,
    pools: SwimPool[],
    dividers: SwimPoolDivider[],
    zoomLevel: ZoomLevel,
    updateLaneIndex: UpdateLaneIndexFunction,
    isExpandedMode: boolean,
): void => {
    const zoomLevelSettings = ZOOM_LEVELS[zoomLevel];
    const startDate = zoomLevelSettings.DATE_FUNCTIONS.base(startOfToday());

    const indexUpdatedItems: ExpandableDataItem[] = [];

    const poolsToDelete = [...pools];

    dataPools.forEach((dataPool) => {
        const existingPool = mappedPools[dataPool.id];

        if (existingPool) {
            // remove pool from deletion list
            poolsToDelete.splice(poolsToDelete.indexOf(existingPool), 1);

            const noodlesToDelete = flatten(existingPool.lanes.map((lane) => lane.noodles));

            dataPool.items.forEach((item) => {
                const existingNoodle = mappedNoodles[`${existingPool.id}-${item.id}`];

                if (existingNoodle && existingNoodle.poolId === existingPool.id) {
                    // remove noodle from deletion list
                    noodlesToDelete.splice(noodlesToDelete.indexOf(existingNoodle), 1);
                    updateNoodle(item, dataPool.id, existingNoodle, startDate, zoomLevel, isExpandedMode, existingPool);
                    checkForOverlap(item, existingNoodle, existingPool, indexUpdatedItems);
                } else {
                    const noodle = buildNoodle(item, dataPool.id, startDate, zoomLevel, isExpandedMode);
                    addToLane(item, existingPool, noodle, indexUpdatedItems);
                    mappedNoodles[`${existingPool.id}-${noodle.id}`] = noodle;
                }
            });

            // remove deleted noodles
            noodlesToDelete.forEach((noodle) => {
                if (noodle.poolId === existingPool.id) {
                    const lane = existingPool.lanes.find(({ id }) => id === noodle.laneId);
                    lane?.expandedNoodles.splice(
                        lane.expandedNoodles.findIndex(({ id }) => id === noodle.id),
                        1,
                    );
                    lane?.noodles.splice(lane.noodles.indexOf(noodle), 1);
                    delete mappedNoodles[`${existingPool.id}-${noodle.id}`];
                    noodle.impl?.group.remove();
                }
            });

            calculateLanesY(existingPool.lanes);
        } else {
            const { swimPool, divider } = buildPool(
                dataPool,
                startDate,
                zoomLevel,
                indexUpdatedItems,
                mappedNoodles,
                isExpandedMode,
            );
            pools.push(swimPool);
            dividers.push(divider);
            mappedPools[dataPool.id] = swimPool;
            mappedDividers[dataPool.id] = divider;
        }
    });

    // Remove deleted pools
    poolsToDelete.forEach((pool) => {
        const divider = mappedDividers[pool.id];
        dividers.splice(dividers.indexOf(divider), 1);
        delete mappedDividers[pool.id];
        divider.lineImpl?.group.remove();
        divider.nameImpl?.group.remove();

        pool.lanes.forEach((lane) => {
            lane.noodles.forEach((noodle) => {
                delete mappedNoodles[`${pool.id}-${noodle.id}`];
                noodle.impl?.group.remove();
            });
            lane.impl?.group.remove();
        });
        pools.splice(pools.indexOf(pool), 1);
        delete mappedPools[pool.id];
        pool.impl?.group.remove();
    });

    pools.sort((a, b) => (a.order || 0) - (b.order || 0));
    dividers.sort((a, b) => (a.order || 0) - (b.order || 0));

    // Calculate Y positions
    let nextPoolY = 0;

    pools.forEach((swimPool) => {
        const divider = mappedDividers[swimPool.id];

        // The pool y position is based on the position and height of the previous pool
        const y = nextPoolY;

        // The next pool y is based on this pool's height plus some offsets
        nextPoolY += swimPool.height + SPACER_HEIGHT;

        swimPool.y = y;
        divider.y = y;
        divider.height = swimPool.height;

        swimPool.impl?.onTransition({ height: swimPool.height, y: swimPool.y });
        divider.lineImpl?.onTransition({ y: divider.y, height: divider.height });
        divider.nameImpl?.onTransition({ y: divider.y });
    });

    if (indexUpdatedItems.length > 0) {
        indexUpdatedItems.forEach((item) => updateLaneIndex(item, item.laneIndex || 0));
    }
};

export const resizeNoodles = (pools: SwimPool[], zoomLevel: ZoomLevel): SwimPool[] => {
    const zoomLevelSettings = ZOOM_LEVELS[zoomLevel];
    const startDate = zoomLevelSettings.DATE_FUNCTIONS.base(startOfToday());
    pools.forEach((pool) =>
        pool.lanes.forEach((lane) =>
            lane.noodles.forEach((noodle) => {
                noodle.bathtub.forEach((bathtubLane) =>
                    bathtubLane.ducks.forEach((duck) => {
                        const start = (duck.item.start || duck.item.end) as Date;
                        const end = (duck.item.end || duck.item.start) as Date;

                        const { x, width } = getXAndWidth(start, end, noodle.item.drawStart, zoomLevel);
                        duck.x = x - 1;
                        duck.width = width;
                        if (noodle.isExpanded) {
                            duck.impl?.onTransition({ x, width });
                        } else {
                            duck.impl?.setXAndWidth({ x, width });
                        }
                    }),
                );
                const { x, width, innerX, innerWidth } = getNoodleCoordinates(noodle.item, startDate, zoomLevel);
                noodle.x = x;
                noodle.width = width;
                noodle.innerX = innerX;
                noodle.innerWidth = innerWidth;
                noodle.impl?.onTransition({ x, width, innerX, innerWidth });
            }),
        ),
    );

    return [...pools];
};

export const toggleNoodle = (noodle: PoolNoodle, pools: SwimPool[], dividers: SwimPoolDivider[]): void => {
    // If the noodle has no bathtub, then there iss no need to do anything
    if (noodle.bathtub.length === 0) {
        return;
    }

    noodle.isExpanded = !noodle.isExpanded;

    if (noodle.isExpanded) {
        // We add the panel height to the existing noodle height (usually NOODLE_HEIGHT)
        noodle.height += noodle.bathtub.length * SWIMLANE_Y_OFFSET;
    } else {
        noodle.height = NOODLE_HEIGHT;
    }
    noodle.impl?.onToggle();

    let nextPoolY = 0;

    pools.forEach((pool, index) => {
        const divider = dividers[index];

        if (noodle.poolId === pool.id) {
            let poolHeight = SWIMLANE_Y_OFFSET;
            let nextLaneY = SWIMLANE_Y_OFFSET;
            pool.lanes.forEach((lane) => {
                if (noodle.laneId === lane.id) {
                    if (noodle.isExpanded) {
                        lane.expandedNoodles.push({ id: noodle.id, height: noodle.height });
                    } else {
                        lane.expandedNoodles = lane.expandedNoodles.filter(({ id }) => id !== noodle.id);
                    }
                    const largestNoodleHeight = getLargestNoodleHeight(lane.expandedNoodles);

                    // Lane height will be the largest noodle height plus some offsets
                    lane.height = calculateLaneHeight(largestNoodleHeight);
                }
                lane.y = nextLaneY;
                lane.impl?.onTransition({ height: lane.height, y: lane.y });

                // Next lane y and pool height need some offsets separately
                const laneDelta = lane.height + SPACER_HEIGHT;
                nextLaneY = lane.y + laneDelta;
                poolHeight += laneDelta;
            });
            pool.height = poolHeight;
            divider.height = poolHeight;
        }

        pool.y = nextPoolY;
        pool.impl?.onTransition({ height: pool.height, y: pool.y });

        divider.y = pool.y;
        divider.lineImpl?.onTransition({ y: divider.y, height: divider.height });
        divider.nameImpl?.onTransition({ y: divider.y });

        // The next pool y is based on this pool's height plus some offsets
        nextPoolY += pool.height + SPACER_HEIGHT;
    });
};

export const toggleAllNoodles = (pools: SwimPool[], dividers: SwimPoolDivider[], isExpanded: boolean): void => {
    let nextPoolY = 0;

    pools.forEach((pool, index) => {
        const divider = dividers[index];

        let poolHeight = SWIMLANE_Y_OFFSET;
        let nextLaneY = SWIMLANE_Y_OFFSET;
        pool.lanes.forEach((lane) => {
            lane.noodles.forEach((noodle) => {
                noodle.isExpanded = isExpanded;

                if (noodle.isExpanded) {
                    noodle.height = NOODLE_HEIGHT + noodle.bathtub.length * SWIMLANE_Y_OFFSET;
                    lane.expandedNoodles.push({ id: noodle.id, height: noodle.height });
                } else {
                    noodle.height = NOODLE_HEIGHT;
                    lane.expandedNoodles = lane.expandedNoodles.filter(({ id }) => id !== noodle.id);
                }

                noodle.impl?.onToggle();
            });

            const largestNoodleHeight = getLargestNoodleHeight(lane.expandedNoodles);
            // Lane height will be the largest noodle height plus some offsets
            lane.height = calculateLaneHeight(largestNoodleHeight);

            lane.y = nextLaneY;
            lane.impl?.onTransition({ height: lane.height, y: lane.y });

            // Next lane y and pool height need some offsets separately
            const laneDelta = lane.height + SPACER_HEIGHT;
            nextLaneY = lane.y + laneDelta;
            poolHeight += laneDelta;
        });

        pool.height = poolHeight || SWIMLANE_Y_OFFSET;
        divider.height = pool.height;

        pool.y = nextPoolY;
        pool.impl?.onTransition({ height: pool.height, y: pool.y });

        divider.y = pool.y;
        divider.lineImpl?.onTransition({ y: divider.y, height: divider.height });
        divider.nameImpl?.onTransition({ y: divider.y });

        // The next pool y is based on this pool's height plus some offsets
        nextPoolY += pool.height + SPACER_HEIGHT;
    });
};
