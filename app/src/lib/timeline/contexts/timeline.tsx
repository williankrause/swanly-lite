import { noop } from 'lodash';
import { createContext, FunctionComponent, PropsWithChildren, useCallback, useContext } from 'react';
import { useSetState } from 'react-use';
import {
    OnMoveToTodayFunction,
    RecreatePoolsFunction,
    SetOnMoveToTodayFunction,
    SetRecreatePoolsFunction,
} from '../types/base';

export type LimitDates = {
    startDate: Date;
    endDate: Date;
};

export type SetLimitDatesFunction = (limitDates: LimitDates) => void;

interface StateType {
    onMoveToToday: OnMoveToTodayFunction;
    recreatePools: RecreatePoolsFunction;
    limitDates: LimitDates | null;
}

interface TimelineContextType extends StateType {
    limitDates: LimitDates | null;
    setLimitDates: SetLimitDatesFunction;
    setOnMoveToToday: SetOnMoveToTodayFunction;
    setRecreatePools: SetRecreatePoolsFunction;
}

const TimelineContext = createContext<TimelineContextType>({
    limitDates: null,
    setLimitDates: noop,
    onMoveToToday: noop,
    setOnMoveToToday: noop,
    recreatePools: noop,
    setRecreatePools: noop,
});

export const TimelineProvider: FunctionComponent<PropsWithChildren<unknown>> = ({ children }) => {
    const [state, setState] = useSetState<StateType>({ onMoveToToday: noop, recreatePools: noop, limitDates: null });

    const setOnMoveToToday = useCallback((onMoveToToday: OnMoveToTodayFunction) => setState({ onMoveToToday }), [
        setState,
    ]);

    const setRecreatePools = useCallback((recreatePools: RecreatePoolsFunction) => setState({ recreatePools }), [
        setState,
    ]);

    const setLimitDates = useCallback((limitDates: LimitDates) => setState({ limitDates }), [setState]);

    return (
        <TimelineContext.Provider value={{ ...state, setLimitDates, setOnMoveToToday, setRecreatePools }}>
            {children}
        </TimelineContext.Provider>
    );
};

export const useTimelineEvents: () => TimelineContextType = () => useContext(TimelineContext);
