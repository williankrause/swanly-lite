import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { calculateDuration } from 'lib/timeline/handlers/common';
import { Theme } from 'lib/timeline/theme';
import PoolNoodleImpl, { NoodleNameClickFunction, NoodleToggleFunction, PoolNoodle } from './PoolNoodle';

export type ExpandedNoodle = { id: string; height: number };

export interface SwimLane {
    id: string;
    y?: number;
    height: number;
    noodles: PoolNoodle[];
    expandedNoodles: Array<ExpandedNoodle>;
    impl?: SwimLaneImpl;
}

type SwimLaneTransition = {
    height?: number;
    y?: number;
};

export default class SwimLaneImpl {
    group: Group;

    poolNoodles: PoolNoodleImpl[] = [];

    constructor(
        lane: SwimLane,
        onNoodleToggle: NoodleToggleFunction,
        onNoodleNameClick: NoodleNameClickFunction,
        theme: Theme,
        stageX: number,
    ) {
        this.group = new Konva.Group({
            id: lane.id,
            y: lane.y,
            height: lane.height,
        });

        lane.noodles.forEach((noodle) => {
            const impl = new PoolNoodleImpl(noodle, onNoodleToggle, onNoodleNameClick, theme, stageX);
            this.poolNoodles.push(impl);
            this.group.add(impl.group);
        });

        // eslint-disable-next-line no-param-reassign
        lane.impl = this;
    }

    onTransition = ({ height, y }: SwimLaneTransition): void => {
        if (!this.group.parent) {
            return;
        }

        if (height) {
            const currentHeight = this.group.height();
            this.group.to({
                height,
                duration: calculateDuration(height - currentHeight),
            });
        }

        if (y) {
            const currentY = this.group.y();
            this.group.to({
                y,
                duration: calculateDuration(y - currentY),
            });
        }
    };

    onScroll = (newX: number): void => {
        this.poolNoodles.forEach((noodle) => {
            noodle.onScroll(newX);
        });
    };
}
