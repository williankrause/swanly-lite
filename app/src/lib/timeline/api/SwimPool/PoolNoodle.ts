import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { KonvaEventObject } from 'konva/lib/Node';
import { Image } from 'konva/lib/shapes/Image';
import { Label } from 'konva/lib/shapes/Label';
import { Rect } from 'konva/lib/shapes/Rect';
import { Text } from 'konva/lib/shapes/Text';
import {
    SWIMLANE_PADDING,
    NOODLE_RADIUS,
    NOODLE_SHADOW_BLUR,
    NOODLE_SHADOW_OFFSET,
    NOODLE_HOVERED_SHADOW_OFFSET,
    NOODLE_FONT_STYLE,
    NOODLE_FONT_SIZE,
    NOODLE_TEXT_PADDING,
    NOODLE_HOVERED_SHADOW_BLUR,
    ANIMATION_DURATION,
    NOODLE_HEIGHT,
    SWIMPOOL_DIVIDER_NAME_RADIUS,
    ICON_OFFSET,
    STATUS_ICON_SIZE,
} from 'lib/timeline/constants';
import { calculateDuration } from 'lib/timeline/handlers/common';
import { Theme } from 'lib/timeline/theme';
import { ExpandableDataItem } from 'lib/timeline/types/base';
import { lighten } from 'polished';
import BathtubImpl from '../Bathtub/Bathtub';
import { BathtubLane } from '../Bathtub/BathtubLane';

export interface PoolNoodle {
    id: string;
    x: number;
    width: number;
    innerX?: number;
    innerWidth?: number;
    height: number;
    text: string;
    isExpanded: boolean;
    color?: string;
    fontColor?: string;
    bathtub: BathtubLane[];
    poolId: string;
    item: ExpandableDataItem;
    laneId?: string;
    icon?: string;
    impl?: PoolNoodleImpl;
}

export type NoodleToggleFunction = (noodle: PoolNoodle) => void;
export type NoodleNameClickFunction = (item: ExpandableDataItem) => void;
export type ItemToggleFunction = (item: ExpandableDataItem, isExpanded: boolean) => void;

type PoolNoodleTransition = {
    x?: number;
    width?: number;
    innerX?: number;
    innerWidth?: number;
    height?: number;
};

export default class PoolNoodleImpl {
    group: Group;

    rect: Rect;

    innerRect?: Rect;

    text: Text;

    tooltipLabel: Label;

    bathtub: BathtubImpl;

    noodle: PoolNoodle;

    icon?: Image;

    theme: Theme;

    isTextHidden = false;

    constructor(
        noodle: PoolNoodle,
        onToggle: NoodleToggleFunction,
        onNameClick: NoodleNameClickFunction,
        theme: Theme,
        stageX: number,
    ) {
        this.noodle = noodle;
        this.theme = theme;
        const { NOODLE_TRANSPARENT_COLOR, NOODLE_DEFAULT_COLOR, NOODLE_FONT_COLOR } = theme;
        const color = noodle.color || NOODLE_DEFAULT_COLOR;
        const iconX = noodle.width - ICON_OFFSET;

        this.group = new Konva.Group({
            id: noodle.id,
            x: noodle.x,
            y: SWIMLANE_PADDING,
            width: noodle.width,
            height: noodle.height,
        });

        this.rect = new Konva.Rect({
            id: `${noodle.id}_rect`,
            width: noodle.width,
            height: NOODLE_HEIGHT,
            cornerRadius: NOODLE_RADIUS,
            shadowBlur: NOODLE_HOVERED_SHADOW_BLUR,
            shadowColor: NOODLE_TRANSPARENT_COLOR,
            shadowOffsetY: NOODLE_HOVERED_SHADOW_OFFSET,
            shadowOpacity: 0,
            fill: lighten(0.2, color),
            perfectDrawEnabled: false,
        });

        if (noodle.innerWidth && noodle.innerX) {
            this.innerRect = new Konva.Rect({
                id: `${noodle.id}_inner_rect`,
                x: noodle.innerX,
                width: noodle.innerWidth,
                height: NOODLE_HEIGHT,
                cornerRadius: NOODLE_RADIUS,
                shadowBlur: NOODLE_SHADOW_BLUR,
                shadowColor: NOODLE_TRANSPARENT_COLOR,
                shadowOffsetY: NOODLE_SHADOW_OFFSET,
                fillLinearGradientStartPoint: { x: 0, y: 0 },
                fillLinearGradientEndPoint: { x: noodle.innerWidth, y: NOODLE_HEIGHT },
                fillLinearGradientColorStops: [0, color, 1, lighten(0.1, color)],
                perfectDrawEnabled: false,
            });
        }

        this.text = new Konva.Text({
            id: `${noodle.id}_text`,
            fontStyle: NOODLE_FONT_STYLE,
            fontSize: NOODLE_FONT_SIZE,
            padding: NOODLE_TEXT_PADDING,
            text: noodle.text,
            fill: NOODLE_FONT_COLOR,
            ellipsis: true,
            wrap: 'none',
            perfectDrawEnabled: false,
            x: this.getTextX(stageX),
        });

        if (noodle.icon) {
            Konva.Image.fromURL(noodle.icon, (icon: Image) => {
                icon.setAttrs({
                    x: iconX,
                    y: NOODLE_TEXT_PADDING / 2,
                    width: STATUS_ICON_SIZE,
                    height: STATUS_ICON_SIZE,
                });
                this.icon = icon;
                this.icon.transformsEnabled('position');
                this.group.add(this.icon);
            });
        }

        this.tooltipLabel = new Konva.Label({
            id: `${noodle.id}_tooltip_label`,
            y: 0,
        });
        this.tooltipLabel.on('click tap', (evt) => {
            // eslint-disable-next-line no-param-reassign
            evt.cancelBubble = true;
            onNameClick(this.noodle.item);
        });

        const tag = new Konva.Tag({
            id: `${noodle.id}_tooltip_tag`,
            fill: theme.TAG_FILL,
            perfectDrawEnabled: false,
            listening: false,
            cornerRadius: SWIMPOOL_DIVIDER_NAME_RADIUS,
        });
        tag.transformsEnabled('none');

        this.tooltipLabel.add(tag);

        this.bathtub = new BathtubImpl(noodle.bathtub, noodle.width, noodle.height, noodle.isExpanded, theme);

        if (noodle.isExpanded) {
            this.group.add(this.bathtub.group);
        }

        this.group.add(this.rect);

        if (this.innerRect) {
            this.group.add(this.innerRect);
        }

        if (noodle.width >= this.text.width()) {
            this.group.add(this.text);
        } else {
            this.isTextHidden = true;
        }

        this.group.on('mouseenter', this.onMouseEnter);
        this.group.on('mouseleave', this.onMouseLeave);
        this.group.on('click tap', () => {
            if (this.bathtub.lanes.length) {
                onToggle(this.noodle);
            } else {
                onNameClick(this.noodle.item);
            }
        });

        this.text.on('click tap', (evt: KonvaEventObject<MouseEvent>) => {
            // eslint-disable-next-line no-param-reassign
            evt.cancelBubble = true;
            onNameClick(this.noodle.item);
        });

        // eslint-disable-next-line no-param-reassign
        noodle.impl = this;
    }

    updateTheme = (theme: Theme): void => {
        this.bathtub.updateTheme(theme);
    };

    getTextX = (stageX: number): number => {
        const { x: start, width } = this.noodle;
        const end = start + width;

        if (stageX >= start && stageX <= end) {
            const textWidth = this.text?.width() || 0;
            const difference = stageX - start;

            return Math.min(difference, width - textWidth - ICON_OFFSET);
        }

        return 0;
    };

    redraw = (): void => {
        this.group.getStage()?.batchDraw();
    };

    onMouseEnter = (): void => {
        this.rect.shadowOpacity(0.6);
        if (this.innerRect) {
            this.innerRect.shadowOpacity(0.6);
        }
        const stage = this.group.getStage();
        if (stage) {
            stage.container().style.cursor = 'pointer';
        }

        if (this.isTextHidden) {
            this.tooltipLabel.add(this.text);
            this.group.add(this.tooltipLabel);
            if (this.tooltipLabel.parent) {
                this.tooltipLabel.to({
                    y: -NOODLE_HEIGHT,
                    duration: 0.2,
                });
            }
        }

        this.redraw();
    };

    onMouseLeave = (): void => {
        this.rect.shadowOpacity(0);
        if (this.innerRect) {
            this.innerRect.shadowOpacity(1);
        }
        const stage = this.group.getStage();
        if (stage) {
            stage.container().style.cursor = 'default';
        }
        if (this.isTextHidden && this.tooltipLabel.parent) {
            this.tooltipLabel.to({
                y: 0,
                duration: 0.2,
                onFinish: () => {
                    this.text.remove();
                    this.tooltipLabel.remove();
                },
            });
        }
        this.redraw();
    };

    onToggle = (): void => {
        this.bathtub.setHeight(this.noodle.height);
        const currentHeight = this.group.height();
        const duration = calculateDuration(currentHeight - this.noodle.height);

        this.group.to({
            height: this.noodle.height,
            duration,
        });

        if (this.noodle.isExpanded) {
            this.group.removeChildren();
            this.group.add(this.bathtub.group).add(this.rect);

            if (this.innerRect) {
                this.group.add(this.innerRect);
            }

            if (!this.isTextHidden) {
                this.group.add(this.text);
                if (this.icon) {
                    this.group.add(this.icon);
                }
            }
            this.group.draw();

            this.bathtub.toggleVisible(this.noodle.isExpanded);
        } else {
            this.bathtub.toggleVisible(this.noodle.isExpanded, () => this.bathtub.group.remove());
        }
    };

    onTransition = ({ x, width, height, innerX, innerWidth }: PoolNoodleTransition): void => {
        const groupWidth = this.group.width();
        const iconX = (width || groupWidth) - ICON_OFFSET;

        if (this.group.parent) {
            if (x) {
                this.group.to({ x, duration: ANIMATION_DURATION.ZOOM });
                if (this.icon) {
                    if (this.icon.parent) {
                        this.icon.to({ x: iconX, duration: ANIMATION_DURATION.ZOOM });
                    } else {
                        this.icon.x(iconX);
                    }
                }
            }

            if (innerX) {
                this.innerRect?.to({ x: innerX, duration: ANIMATION_DURATION.ZOOM });
            }
            if (innerWidth) {
                this.innerRect?.to({ width: innerWidth, duration: ANIMATION_DURATION.ZOOM });
            }

            if (width) {
                // If text will overflow, then hide it
                if (width >= this.text.width()) {
                    if (this.isTextHidden) {
                        this.isTextHidden = false;
                        this.group.add(this.text);
                        if (this.icon) {
                            this.group.add(this.icon);
                        }
                        this.text.to({ width: iconX - NOODLE_TEXT_PADDING, duration: ANIMATION_DURATION.ZOOM });
                    }
                } else {
                    this.text.remove();
                    this.icon?.remove();
                    this.isTextHidden = true;
                }

                // because of how animations are implemented inside Konva, we can't create a single
                // object and share between transitions...
                this.group.to({ width, duration: ANIMATION_DURATION.ZOOM });
                this.rect.to({ width, duration: ANIMATION_DURATION.ZOOM });

                if (this.noodle.isExpanded) {
                    this.bathtub.onTransition({ width });
                } else {
                    this.bathtub.setWidth(width);
                }
            }

            if (height) {
                this.group.to({ height, duration: ANIMATION_DURATION.ZOOM });
                if (this.noodle.isExpanded) {
                    this.bathtub.onTransition({ height });
                } else {
                    this.bathtub.setHeight(height);
                }
            }
        } else {
            if (x) {
                this.group.x(x);
                this.icon?.x(x);
            }

            if (innerX) {
                this.innerRect?.x(innerX);
            }
            if (innerWidth) {
                this.innerRect?.width(innerWidth);
            }

            if (width) {
                // If text will overflow, then hide it
                if (width >= this.text.width()) {
                    if (this.isTextHidden) {
                        this.isTextHidden = false;
                        this.group.add(this.text);
                        if (this.icon) {
                            this.group.add(this.icon);
                        }
                        this.text.width(iconX - NOODLE_TEXT_PADDING);
                    }
                } else {
                    this.text.remove();
                    this.icon?.remove();
                    this.isTextHidden = true;
                }

                // because of how animations are implemented inside Konva, we can't create a single
                // object and share between transitions...
                this.group.width(width);
                this.rect.width(width);

                if (this.noodle.isExpanded) {
                    this.bathtub.onTransition({ width });
                } else {
                    this.bathtub.setWidth(width);
                }
            }

            if (height) {
                this.group.height(height);
                if (this.noodle.isExpanded) {
                    this.bathtub.onTransition({ height });
                } else {
                    this.bathtub.setHeight(height);
                }
            }
        }
    };

    onUpdateNoodle = (iconChanged: boolean): void => {
        this.onTransition({
            x: this.noodle.x,
            width: this.noodle.width,
            height: this.noodle.height,
            innerX: this.noodle.innerX,
            innerWidth: this.noodle.innerWidth,
        });

        this.text.text(this.noodle.text);

        const iconX = this.noodle.width - ICON_OFFSET;

        if (iconChanged) {
            this.icon?.remove();
            if (this.noodle.icon) {
                Konva.Image.fromURL(this.noodle.icon, (icon: Image) => {
                    icon.setAttrs({
                        x: iconX,
                        y: NOODLE_TEXT_PADDING / 2,
                        width: STATUS_ICON_SIZE,
                        height: STATUS_ICON_SIZE,
                    });
                    this.icon = icon;
                    this.icon.transformsEnabled('position');
                    this.group.add(this.icon);
                });
            }
        }

        const { NOODLE_DEFAULT_COLOR } = this.theme;
        const color = this.noodle.color || NOODLE_DEFAULT_COLOR;
        this.rect.fill(lighten(0.2, color));
        this.innerRect?.fillLinearGradientColorStops([0, color, 1, lighten(0.1, color)]);

        this.bathtub.updateBathtub(this.noodle.bathtub);
    };

    onScroll = (newX: number): void => {
        this.text.x(this.getTextX(newX));
    };
}
