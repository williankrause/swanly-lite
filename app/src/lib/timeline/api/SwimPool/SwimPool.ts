import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { calculateDuration } from 'lib/timeline/handlers/common';
import { Theme } from 'lib/timeline/theme';
import { NoodleNameClickFunction, NoodleToggleFunction } from './PoolNoodle';
import SwimLaneImpl, { SwimLane } from './SwimLane';

export interface SwimPool {
    id: string;
    y?: number;
    height: number;
    lanes: SwimLane[];
    order?: number;
    impl?: SwimPoolImpl;
}

type SwimPoolTransition = {
    height?: number;
    y?: number;
};

export default class SwimPoolImpl {
    group: Group;

    lanes: SwimLaneImpl[] = [];

    constructor(
        pool: SwimPool,
        onNoodleToggle: NoodleToggleFunction,
        onNoodleNameClick: NoodleNameClickFunction,
        theme: Theme,
        stageX: number,
    ) {
        this.group = new Konva.Group({
            id: pool.id,
            y: pool.y,
            height: pool.height,
        });

        pool.lanes.forEach((lane) => {
            const impl = new SwimLaneImpl(lane, onNoodleToggle, onNoodleNameClick, theme, stageX);
            this.lanes.push(impl);
            this.group.add(impl.group);
        });

        // eslint-disable-next-line no-param-reassign
        pool.impl = this;
    }

    onTransition = ({ height, y }: SwimPoolTransition): void => {
        if (!this.group.parent) {
            return;
        }

        if (height) {
            const currentHeight = this.group.height();
            this.group.to({
                height,
                duration: calculateDuration(height - currentHeight),
            });
        }

        if (y) {
            const currentY = this.group.y();
            this.group.to({
                y,
                duration: calculateDuration(y - currentY),
            });
        }
    };

    onScroll = (newX: number): void => {
        this.lanes.forEach((lane) => {
            lane.onScroll(newX);
        });
    };
}
