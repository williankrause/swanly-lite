import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { Rect } from 'konva/lib/shapes/Rect';
import { SWIMPOOL_DIVIDER_LINE_HEIGHT } from 'lib/timeline/constants';
import { calculateDuration } from 'lib/timeline/handlers/common';

export interface BaseSwimPoolDividerLine {
    id: string;
    y?: number;
    height: number;
}

type SwimPoolDividerLineTransition = {
    y?: number;
    height?: number;
};

export default class SwimPoolDividerLineImpl {
    group: Group;

    rect: Rect;

    constructor(divider: BaseSwimPoolDividerLine, width: number, color: string) {
        this.group = new Konva.Group({
            id: divider.id,
            y: divider.y,
        });
        this.group.transformsEnabled('position');

        this.rect = new Konva.Rect({
            id: `${divider.id}_rect`,
            y: divider.height,
            width,
            height: SWIMPOOL_DIVIDER_LINE_HEIGHT,
            fill: color,
            perfectDrawEnabled: false,
            listening: false,
        });

        this.group.add(this.rect);
    }

    onTransition = ({ y, height }: SwimPoolDividerLineTransition): void => {
        if (y) {
            const currentY = this.group.y();
            this.group.to({
                y,
                duration: calculateDuration(y - currentY),
            });
        }

        if (height) {
            const currentY = this.rect.y();
            this.rect.to({
                y: height,
                duration: calculateDuration(height - currentY),
            });
        }
    };
}
