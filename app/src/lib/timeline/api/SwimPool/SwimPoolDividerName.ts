import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import {
    SWIMPOOL_AVATAR_PADDING,
    SWIMPOOL_AVATAR_SIZE,
    SWIMPOOL_NAME_HORIZONTAL_PADDING,
    SWIMPOOL_DIVIDER_NAME_RADIUS,
    SPACER_HEIGHT,
} from 'lib/timeline/constants';
import { calculateDuration } from 'lib/timeline/handlers/common';
import { Theme } from 'lib/timeline/theme';

export interface BaseSwimPoolDividerName {
    id: string;
    y?: number;
    icon?: string;
    text: string;
}

type SwimPoolDividerNameTransition = {
    y: number;
};

export default class SwimPoolDividerNameImpl {
    group: Group;

    tag: Konva.Tag;

    text: Konva.Text;

    constructor(divider: BaseSwimPoolDividerName, theme: Theme) {
        this.group = new Konva.Group({
            id: `${divider.id}_name`,
            y: divider.y,
        });
        this.group.transformsEnabled('position');

        if (divider.icon) {
            const img = document.createElement('img');
            img.onload = () => {
                const image = new Konva.Image({
                    x: SWIMPOOL_AVATAR_PADDING,
                    width: SWIMPOOL_AVATAR_SIZE,
                    height: SWIMPOOL_AVATAR_SIZE,
                    image: img,
                    listening: false,
                });
                this.group.add(image);
                this.redraw();
            };
            img.src = divider.icon;
        }

        const label = new Konva.Label({
            id: `${divider.id}_label`,
            x: SWIMPOOL_NAME_HORIZONTAL_PADDING,
            y: SPACER_HEIGHT / 2,
        });

        const tag = new Konva.Tag({
            id: `${divider.id}_tag`,
            fill: theme.TAG_FILL,
            perfectDrawEnabled: false,
            listening: false,
            cornerRadius: SWIMPOOL_DIVIDER_NAME_RADIUS,
        });
        tag.transformsEnabled('none');
        this.tag = tag;

        label.add(tag);

        const text = new Konva.Text({
            id: `${divider.id}_text`,
            text: divider.text,
            fill: theme.NOODLE_FONT_COLOR_DARK,
            padding: SPACER_HEIGHT / 4,
            perfectDrawEnabled: false,
        });
        text.transformsEnabled('none');
        this.text = text;
        label.add(text);

        this.group.add(label);

        this.group.on('mouseenter', this.onMouseEnter);
        this.group.on('mouseleave', this.onMouseLeave);
    }

    updateTheme = (theme: Theme): void => {
        this.tag.to({ fill: theme.TAG_FILL });
        this.text.to({ fill: theme.NOODLE_FONT_COLOR_DARK });
    };

    redraw = (): void => {
        this.group.getStage()?.batchDraw();
    };

    onMouseEnter = (): void => {
        const stage = this.group.getStage();
        if (stage) {
            stage.container().style.cursor = 'pointer';
        }
    };

    onMouseLeave = (): void => {
        const stage = this.group.getStage();
        if (stage) {
            stage.container().style.cursor = 'default';
        }
    };

    onTransition = ({ y }: SwimPoolDividerNameTransition): void => {
        const currentY = this.group.y();
        this.group.to({
            y,
            duration: calculateDuration(y - currentY),
        });
        this.redraw();
    };
}
