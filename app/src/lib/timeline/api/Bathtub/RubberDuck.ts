import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { Image } from 'konva/lib/shapes/Image';
import { Rect } from 'konva/lib/shapes/Rect';
import { Text } from 'konva/lib/shapes/Text';
import { darken } from 'polished';
import {
    ANIMATION_DURATION,
    ICON_OFFSET,
    NOODLE_FONT_SIZE,
    NOODLE_FONT_STYLE,
    NOODLE_RADIUS,
    NOODLE_TEXT_PADDING,
    RUBBER_DUCK_FONT_SIZE,
    RUBBER_DUCK_LINE_HEIGHT,
    STATUS_ICON_SIZE,
    SWIMLANE_PADDING,
} from 'lib/timeline/constants';
import { DataItem } from 'lib/timeline/types/base';
import { Theme } from 'lib/timeline/theme';

export interface RubberDuck {
    id: string;
    x: number;
    width: number;
    text: string;
    color?: string;
    icon?: string;
    item: DataItem;
    impl?: RubberDuckImpl;
}

type RubberDuckTransition = {
    width?: number;
    x?: number;
};

export default class RubberDuckImpl {
    group: Group;

    rect: Rect;

    text: Text;

    icon?: Image;

    duck: RubberDuck;

    theme: Theme;

    constructor(rubberDuck: RubberDuck, theme: Theme) {
        this.theme = theme;
        const { NOODLE_DEFAULT_COLOR, NOODLE_FONT_COLOR_DARK } = theme;
        const iconX = rubberDuck.width - ICON_OFFSET;
        const color = rubberDuck.color || NOODLE_DEFAULT_COLOR;

        this.group = new Konva.Group({
            id: rubberDuck.id,
            x: rubberDuck.x,
            y: SWIMLANE_PADDING,
            width: rubberDuck.width,
        });

        this.text = new Konva.Text({
            id: `${rubberDuck.id}_text`,
            x: NOODLE_TEXT_PADDING,
            width: iconX - NOODLE_TEXT_PADDING,
            text: rubberDuck.text,
            fontSize: RUBBER_DUCK_FONT_SIZE,
            fontStyle: NOODLE_FONT_STYLE,
            fill: NOODLE_FONT_COLOR_DARK,
            ellipsis: true,
            wrap: 'none',
            perfectDrawEnabled: false,
            listening: false,
        });

        this.rect = new Konva.Rect({
            y: SWIMLANE_PADDING * 2 + NOODLE_FONT_SIZE,
            width: rubberDuck.width,
            height: RUBBER_DUCK_LINE_HEIGHT,
            cornerRadius: NOODLE_RADIUS,
            fillLinearGradientStartPoint: { x: 0, y: 0 },
            fillLinearGradientEndPoint: { x: rubberDuck.width, y: 0 },
            fillLinearGradientColorStops: [0, color, 1, darken(0.05, color)],
            perfectDrawEnabled: false,
            listening: false,
        });

        this.group.add(this.text).add(this.rect);

        if (rubberDuck.icon) {
            Konva.Image.fromURL(rubberDuck.icon, (icon: Image) => {
                icon.setAttrs({
                    x: iconX,
                    width: STATUS_ICON_SIZE,
                    height: STATUS_ICON_SIZE,
                });
                this.icon = icon;
                this.icon.transformsEnabled('position');
                this.group.add(this.icon);
            });
        }

        // eslint-disable-next-line no-param-reassign
        rubberDuck.impl = this;

        this.duck = rubberDuck;
    }

    updateTheme = (theme: Theme, isVisible: boolean): void => {
        if (isVisible) {
            this.text.to({ fill: theme.NOODLE_FONT_COLOR_DARK });
        } else {
            this.text.fill(theme.NOODLE_FONT_COLOR_DARK);
        }
    };

    onTransition = ({ x, width }: RubberDuckTransition): void => {
        const groupWidth = this.group.width();
        const iconX = (width || groupWidth) - ICON_OFFSET;

        if (x) {
            this.group.to({ x, duration: ANIMATION_DURATION.ZOOM });
            this.icon?.to({ x: iconX, duration: ANIMATION_DURATION.ZOOM });
        }

        if (width) {
            this.group.to({ width, duration: ANIMATION_DURATION.ZOOM });
            this.text.to({ width: iconX - NOODLE_TEXT_PADDING, duration: ANIMATION_DURATION.ZOOM });
            this.rect.to({ width, duration: ANIMATION_DURATION.ZOOM });
        }
    };

    setXAndWidth = ({ x, width }: RubberDuckTransition): void => {
        const groupWidth = this.group.width();
        const iconX = (width || groupWidth) - ICON_OFFSET;

        if (x) {
            this.group.x(x);
            this.icon?.x(iconX);
        }

        if (width) {
            this.group.width(width);
            this.text.width(iconX - NOODLE_TEXT_PADDING);
            this.rect.width(width);
        }
    };

    onUpdateDuck = (iconChanged: boolean, isExpanded: boolean): void => {
        if (isExpanded) {
            this.onTransition({
                x: this.duck.x,
                width: this.duck.width,
            });
        } else {
            this.setXAndWidth({ x: this.duck.x, width: this.duck.width });
        }

        this.text.text(this.duck.text);

        const iconX = this.duck.width - ICON_OFFSET;

        if (iconChanged) {
            this.icon?.remove();
            if (this.duck.icon) {
                Konva.Image.fromURL(this.duck.icon, (icon: Image) => {
                    icon.setAttrs({
                        x: iconX,
                        width: STATUS_ICON_SIZE,
                        height: STATUS_ICON_SIZE,
                    });
                    this.icon = icon;
                    this.icon.transformsEnabled('position');
                    this.group.add(this.icon);
                });
            }
        }

        const color = this.duck.color || this.theme.NOODLE_DEFAULT_COLOR;
        this.rect.fillLinearGradientColorStops([0, color, 1, darken(0.05, color)]);
    };
}
