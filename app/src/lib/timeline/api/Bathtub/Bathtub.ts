import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { KonvaEventObject } from 'konva/lib/Node';
import { Rect } from 'konva/lib/shapes/Rect';
import { uniqueId } from 'lodash';
import { transparentize } from 'polished';
import { ANIMATION_DURATION, BATHTUB_PADDING, NOODLE_HEIGHT, NOODLE_RADIUS } from 'lib/timeline/constants';
import { calculateDuration } from 'lib/timeline/handlers/common';
import BathtubLaneImpl, { BathtubLane } from './BathtubLane';
import { Theme } from 'lib/timeline/theme';

const handleGroupClick = (evt: KonvaEventObject<MouseEvent>) => {
    // eslint-disable-next-line no-param-reassign
    evt.cancelBubble = true;
};

type BathtubTransition = {
    width?: number;
    height?: number;
};

export default class BathtubImpl {
    id: string;

    group: Group;

    rect: Rect;

    lanesGroup: Group;

    lanes: BathtubLaneImpl[] = [];

    height: number;

    isVisible: boolean;

    theme: Theme;

    constructor(lanes: BathtubLane[], width: number, height: number, isExpanded: boolean, theme: Theme) {
        this.theme = theme;
        const { BATHTUB_BACKGROUND_COLOR, GRADIENT_STOP } = theme;
        this.isVisible = isExpanded;

        const id = uniqueId('bathtub_');

        this.id = id;
        this.height = height;

        this.group = new Konva.Group({
            id,
        });

        this.rect = new Konva.Rect({
            id: `${id}_rect`,
            width,
            height: isExpanded ? height : 0,
            cornerRadius: NOODLE_RADIUS,
            fillLinearGradientColorStops: [
                0,
                transparentize(0, BATHTUB_BACKGROUND_COLOR),
                1,
                transparentize(0.45, GRADIENT_STOP),
            ],
            fillLinearGradientStartPoint: { x: 0, y: 0 },
            fillLinearGradientEndPoint: { x: 0, y: height },
            perfectDrawEnabled: false,
            listening: false,
        });

        this.lanesGroup = new Konva.Group({
            id: `${id}_lanes`,
            y: isExpanded ? NOODLE_HEIGHT + BATHTUB_PADDING : 0,
            opacity: isExpanded ? 1 : 0,
        });

        lanes.forEach((lane) => {
            const bathtubLane = new BathtubLaneImpl(lane, this.isVisible, theme);
            this.lanes.push(bathtubLane);
            this.lanesGroup.add(bathtubLane.group);
        });

        this.group.add(this.rect).add(this.lanesGroup);

        this.group.on('click tap', handleGroupClick);
    }

    toggleVisible = (isVisible: boolean, onFinish?: () => void): void => {
        this.isVisible = isVisible;

        const currentHeight = this.rect.height();

        const duration = calculateDuration(currentHeight - this.height);

        if (this.group.parent) {
            this.rect.to({
                height: this.height,
                fillLinearGradientEndPointY: this.height,
                duration,
            });

            this.lanesGroup.to({
                y: this.isVisible ? NOODLE_HEIGHT + BATHTUB_PADDING : 0,
                opacity: this.isVisible ? 1 : 0,
                duration,
                onFinish,
            });

            this.lanes.forEach((lane) => lane.toggleVisible(this.isVisible));
        }
    };

    updateTheme = (theme: Theme): void => {
        if (this.isVisible) {
            this.rect.to({
                fillLinearGradientColorStops: [
                    0,
                    transparentize(0, theme.BATHTUB_BACKGROUND_COLOR),
                    1,
                    transparentize(0.45, theme.GRADIENT_STOP),
                ],
            });
        } else {
            this.rect.fillLinearGradientColorStops([
                0,
                transparentize(0, theme.BATHTUB_BACKGROUND_COLOR),
                1,
                transparentize(0.45, theme.GRADIENT_STOP),
            ]);
        }

        this.lanes.forEach((lane) => lane.updateTheme(theme, this.isVisible));
    };

    onTransition = ({ width, height }: BathtubTransition): void => {
        if (this.group.parent) {
            if (width) {
                this.rect.to({
                    width,
                    duration: ANIMATION_DURATION.ZOOM,
                });
            }

            if (height) {
                this.height = height;
                this.rect.to({
                    height,
                    fillLinearGradientEndPointY: height,
                    duration: ANIMATION_DURATION.ZOOM,
                });
            }
        } else if (width) {
            this.rect.width(width);
        }
    };

    setWidth = (width: number): void => {
        this.rect.width(width);
    };

    setHeight = (height: number): void => {
        this.height = height;
    };

    updateBathtub = (lanes: BathtubLane[]): void => {
        this.lanes.forEach((lane) => {
            lane.updateLane();
        });

        lanes.forEach((newLane, index) => {
            if (!this.lanes[index]) {
                const bathtubLane = new BathtubLaneImpl(newLane, this.isVisible, this.theme);
                this.lanes.push(bathtubLane);
                this.lanesGroup.add(bathtubLane.group);
            }
        });
    };
}
