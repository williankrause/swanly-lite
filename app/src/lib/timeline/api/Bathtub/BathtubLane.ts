import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { SWIMLANE_Y_OFFSET } from 'lib/timeline/constants';
import { calculateDuration } from 'lib/timeline/handlers/common';
import { Theme } from 'lib/timeline/theme';
import RubberDuckImpl, { RubberDuck } from './RubberDuck';

export interface BathtubLane {
    id: string;
    y?: number;
    ducks: RubberDuck[];
    impl?: BathtubLaneImpl;
}

export default class BathtubLaneImpl {
    group: Group;

    rubberDucks: RubberDuckImpl[] = [];

    y: number;

    isVisible: boolean;

    theme: Theme;

    lane: BathtubLane;

    constructor(lane: BathtubLane, isVisible: boolean, theme: Theme) {
        this.lane = lane;
        this.theme = theme;

        this.isVisible = isVisible;

        this.y = lane.y || 0;

        this.group = new Konva.Group({
            id: lane.id,
            y: isVisible ? this.y : 0,
            height: isVisible ? SWIMLANE_Y_OFFSET : 0,
            listening: false,
        });

        lane.ducks.forEach((rubberDuck) => {
            const impl = new RubberDuckImpl(rubberDuck, theme);
            this.rubberDucks.push(impl);
            this.group.add(impl.group);
        });

        // eslint-disable-next-line no-param-reassign
        lane.impl = this;
    }

    toggleVisible = (isVisible: boolean): void => {
        this.isVisible = isVisible;

        this.group.to({
            height: this.isVisible ? SWIMLANE_Y_OFFSET : 0,
            duration: calculateDuration(SWIMLANE_Y_OFFSET),
        });

        this.group.to({
            y: this.isVisible ? this.y : 0,
            duration: calculateDuration(this.y),
        });
    };

    updateTheme = (theme: Theme, isVisible: boolean): void => {
        this.rubberDucks.forEach((rubberDuck) => rubberDuck.updateTheme(theme, isVisible));
    };

    updateLane = (): void => {
        this.lane.ducks.forEach((rubberDuck) => {
            if (!rubberDuck.impl) {
                const impl = new RubberDuckImpl(rubberDuck, this.theme);
                this.rubberDucks.push(impl);
                this.group.add(impl.group);
            }
        });
    };
}
