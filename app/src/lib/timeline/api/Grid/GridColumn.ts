import { isWeekend } from 'date-fns';
import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { Rect } from 'konva/lib/shapes/Rect';
import { HEADER_HEIGHT, GRID_LINE_WIDTH, ANIMATION_DURATION } from 'lib/timeline/constants';
import { Theme } from 'lib/timeline/theme';
import { ZoomLevel } from 'lib/timeline/types/base';

export interface BaseGridColumn {
    id: string;
    date: Date;
    x: number;
    width: number;
}

type GridColumnTransition = {
    x?: number;
    width?: number;
};

export default class GridColumnImpl {
    group: Group;

    weekendBackground: Rect;

    date: Date;

    constructor(
        column: BaseGridColumn,
        height: number,
        image: HTMLImageElement | undefined,
        theme: Theme,
        zoomLevel: ZoomLevel,
    ) {
        const id = `${column.id}_id`;
        this.date = column.date;

        this.group = new Konva.Group({
            id,
            x: column.x,
            y: HEADER_HEIGHT,
            width: column.width,
            height,
            listening: false,
        });

        this.weekendBackground = new Konva.Rect({
            id: `${id}_weekend_background`,
            width: column.width,
            height: height - HEADER_HEIGHT,
            fillPatternImage: image,
            fillPatternScale: { x: 1, y: 1 },
            opacity: zoomLevel === ZoomLevel.DAILY && isWeekend(column.date) ? 0.2 : 0,
            perfectDrawEnabled: false,
            listening: false,
        });
        this.group.add(this.weekendBackground);

        const line = new Konva.Rect({
            id: `${id}_line`,
            width: GRID_LINE_WIDTH,
            height: height - HEADER_HEIGHT,
            fill: theme.GRID_LINE_COLOR,
            perfectDrawEnabled: false,
            listening: false,
        });

        line.transformsEnabled('none');

        this.group.add(line);
    }

    onZoomChange = (zoomLevel: ZoomLevel, date: Date, transition: GridColumnTransition): void => {
        this.date = date;

        if (zoomLevel === ZoomLevel.DAILY && isWeekend(this.date)) {
            this.weekendBackground.opacity(1);
        } else {
            this.weekendBackground.opacity(0);
        }

        this.onTransition(transition);
    };

    onTransition = ({ x, width }: GridColumnTransition): void => {
        if (x) {
            this.group.to({
                x,
                duration: ANIMATION_DURATION.ZOOM,
            });
        }

        if (width) {
            this.group.to({
                width,
                duration: ANIMATION_DURATION.ZOOM,
            });
        }
    };
}
