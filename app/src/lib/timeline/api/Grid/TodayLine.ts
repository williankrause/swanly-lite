import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { Rect } from 'konva/lib/shapes/Rect';
import { ANIMATION_DURATION, GRID_LINE_WIDTH, HEADER_HEIGHT } from 'lib/timeline/constants';

interface BaseTodayLine {
    id: string;
    x: number;
}

type TodayLineTransition = {
    x: number;
};

export default class TodayLineImpl {
    group: Group;

    rect: Rect;

    constructor(marker: BaseTodayLine, height: number, todayMarkerColor: string) {
        const id = `${marker.id}_line`;

        this.group = new Konva.Group({
            id,
            x: marker.x,
            y: HEADER_HEIGHT,
            height,
            listening: false,
        });
        this.group.transformsEnabled('position');

        this.rect = new Konva.Rect({
            id: `${id}_rect`,
            width: GRID_LINE_WIDTH,
            height: height - HEADER_HEIGHT,
            fill: todayMarkerColor,
            perfectDrawEnabled: false,
            listening: false,
        });
        this.group.transformsEnabled('position');

        this.group.add(this.rect);
    }

    onTransition = ({ x }: TodayLineTransition): void => {
        this.group.to({
            x,
            duration: ANIMATION_DURATION.ZOOM,
        });
    };
}
