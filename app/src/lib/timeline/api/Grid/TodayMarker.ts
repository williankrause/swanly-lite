import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { Circle } from 'konva/lib/shapes/Circle';
import {
    ANIMATION_DURATION,
    HEADER_HEIGHT,
    TODAY_MARKER_OUTER_OPACITY,
    TODAY_MARKER_RADIUS,
} from 'lib/timeline/constants';

interface BaseTodayMarker {
    id: string;
    x: number;
}

type TodayMarkerTransition = {
    x: number;
};

export default class TodayMarkerImpl {
    group: Group;

    outerCircle: Circle;

    innerCircle: Circle;

    constructor(marker: BaseTodayMarker, color: string) {
        const id = `${marker.id}_marker`;

        this.group = new Konva.Group({
            id,
            x: marker.x,
            y: HEADER_HEIGHT,
            listening: false,
        });
        this.group.transformsEnabled('position');

        this.outerCircle = new Konva.Circle({
            id: `${id}_outer_circle`,
            radius: TODAY_MARKER_RADIUS,
            fill: color,
            opacity: TODAY_MARKER_OUTER_OPACITY,
            perfectDrawEnabled: false,
            listening: false,
        });
        this.outerCircle.transformsEnabled('none');

        this.innerCircle = new Konva.Circle({
            id: `${id}_inner_circle`,
            radius: TODAY_MARKER_RADIUS / 2,
            fill: color,
            perfectDrawEnabled: false,
            listening: false,
        });
        this.innerCircle.transformsEnabled('none');

        this.group.add(this.outerCircle).add(this.innerCircle);
    }

    onTransition = ({ x }: TodayMarkerTransition): void => {
        this.group.to({
            x,
            duration: ANIMATION_DURATION.ZOOM,
        });
    };
}
