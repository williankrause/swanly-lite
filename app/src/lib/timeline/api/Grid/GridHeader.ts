import Konva from 'konva';
import { Group } from 'konva/lib/Group';
import { Rect } from 'konva/lib/shapes/Rect';
import { Text } from 'konva/lib/shapes/Text';
import {
    HEADER_HEIGHT,
    GRID_SHADOW_BLUR,
    GRID_SHADOW_OFFSET_Y,
    GRID_SHADOW_OFFSET_X,
    GRID_TEXT_PADDING,
    GRID_FONT_SIZE,
    GRID_TEXT_SPACING,
    ANIMATION_DURATION,
} from 'lib/timeline/constants';
import { Theme } from 'lib/timeline/theme';

type GridHeaderTexts = {
    firstLine: string;
    secondLine: string;
};

export interface BaseGridHeader {
    id: string;
    x: number;
    width: number;
    header: GridHeaderTexts;
}

type GridHeaderTransition = {
    x?: number;
    width: number;
    newHeader: GridHeaderTexts;
};

export default class GridHeaderImpl {
    group: Group;

    rect: Rect;

    firstLine: Text;

    secondLine: Text;

    constructor(header: BaseGridHeader, theme: Theme) {
        const {
            GRID_TRANSPARENT_COLOR,
            GRID_FILL_COLOR,
            GRID_TEXT_COLOR_FIRST_LINE,
            GRID_TEXT_COLOR_SECOND_LINE,
        } = theme;
        const id = `${header.id}_header`;

        this.group = new Konva.Group({
            id,
            x: header.x,
            width: header.width,
            height: HEADER_HEIGHT,
            listening: false,
        });

        this.rect = new Konva.Rect({
            id: `${id}_border`,
            width: header.width,
            height: HEADER_HEIGHT,
            fill: GRID_FILL_COLOR,
            shadowColor: GRID_TRANSPARENT_COLOR,
            shadowBlur: GRID_SHADOW_BLUR,
            shadowOffsetY: GRID_SHADOW_OFFSET_Y,
            shadowOffsetX: GRID_SHADOW_OFFSET_X,
            perfectDrawEnabled: false,
            listening: false,
        });

        this.firstLine = new Konva.Text({
            id: `${id}_first_line`,
            text: header.header.firstLine,
            padding: GRID_TEXT_PADDING,
            width: header.width,
            fontSize: GRID_FONT_SIZE,
            fill: GRID_TEXT_COLOR_FIRST_LINE,
            perfectDrawEnabled: false,
            listening: false,
        });

        this.secondLine = new Konva.Text({
            id: `${id}_second_line`,
            y: GRID_TEXT_SPACING,
            text: header.header.secondLine,
            padding: GRID_TEXT_PADDING,
            width: header.width,
            fontSize: GRID_FONT_SIZE,
            fill: GRID_TEXT_COLOR_SECOND_LINE,
            perfectDrawEnabled: false,
            listening: false,
        });

        this.group.add(this.rect).add(this.firstLine).add(this.secondLine);
    }

    onTransition = ({ x, width, newHeader }: GridHeaderTransition): void => {
        if (x) {
            this.group.to({
                x,
                duration: ANIMATION_DURATION.ZOOM,
            });
        }

        if (width) {
            this.group.to({
                width,
                duration: ANIMATION_DURATION.ZOOM,
            });
            this.rect.to({
                width,
                duration: ANIMATION_DURATION.ZOOM,
            });
            this.firstLine.to({
                width,
                duration: ANIMATION_DURATION.ZOOM,
            });
            this.secondLine.to({
                width,
                duration: ANIMATION_DURATION.ZOOM,
            });
        }

        if (newHeader) {
            this.firstLine.text(newHeader.firstLine);
            this.secondLine.text(newHeader.secondLine);
        }
    };
}
