/* eslint-disable no-param-reassign */
import Konva from 'konva';
import { Layer } from 'konva/lib/Layer';
import { Stage } from 'konva/lib/Stage';
import { TIMELINE_HEADER_SPACING } from '../constants';
import { Theme } from '../theme';
import { ZoomLevel } from '../types/base';
import { GridColumn } from '../types/gridColumn';
import SwimPoolDivider from '../types/swimPoolDivider';
import { TodayMarker } from '../types/todayMarker';
import GridColumnImpl from './Grid/GridColumn';
import GridHeaderImpl from './Grid/GridHeader';
import TodayLineImpl from './Grid/TodayLine';
import TodayMarkerImpl from './Grid/TodayMarker';
import PoolNoodleImpl, { NoodleNameClickFunction, NoodleToggleFunction } from './SwimPool/PoolNoodle';
import SwimLaneImpl from './SwimPool/SwimLane';
import SwimPoolImpl, { SwimPool } from './SwimPool/SwimPool';
import SwimPoolDividerLineImpl from './SwimPool/SwimPoolDividerLine';
import SwimPoolDividerNameImpl from './SwimPool/SwimPoolDividerName';

export const createPoolsLayer = (
    existingLayer: Layer | null,
    swimPools: SwimPool[],
    onNoodleToggle: NoodleToggleFunction,
    onNoodleNameClick: NoodleNameClickFunction,
    stage: Stage | null,
    theme: Theme,
    stageX: number,
): Layer => {
    const layer =
        existingLayer ||
        new Konva.Layer({
            id: 'main_layer',
            y: TIMELINE_HEADER_SPACING,
        });

    swimPools.forEach((pool) => {
        if (pool.impl) {
            pool.lanes.forEach((lane) => {
                if (lane.impl) {
                    lane.noodles.forEach((noodle) => {
                        if (!noodle.impl) {
                            noodle.impl = new PoolNoodleImpl(noodle, onNoodleToggle, onNoodleNameClick, theme, stageX);
                            lane.impl?.group.add(noodle.impl.group);
                            lane.impl?.poolNoodles.push(noodle.impl);
                        } else {
                            // we need to update the colours when the theme changes. The objects won't be recreated
                            noodle.impl.updateTheme(theme);
                        }
                    });
                } else {
                    lane.impl = new SwimLaneImpl(lane, onNoodleToggle, onNoodleNameClick, theme, stageX);
                    pool.impl?.group.add(lane.impl.group);
                    pool.impl?.lanes.push(lane.impl);
                }
            });
        } else {
            pool.impl = new SwimPoolImpl(pool, onNoodleToggle, onNoodleNameClick, theme, stageX);
            layer.add(pool.impl.group);
        }
    });

    if (!existingLayer) {
        stage?.add(layer);
    }

    return layer;
};

export const createNamesLayer = (
    existingLayer: Layer | null,
    dividers: SwimPoolDivider[],
    pos: number,
    stage: Stage | null,
    theme: Theme,
): Layer => {
    const layer =
        existingLayer ||
        new Konva.Layer({
            id: 'names_layer',
            y: TIMELINE_HEADER_SPACING,
        });

    layer.x(pos);

    dividers.forEach((divider) => {
        if (divider.nameImpl) {
            // we need to update the colours when the theme changes. The objects won't be recreated
            divider.nameImpl.updateTheme(theme);
        } else {
            divider.nameImpl = new SwimPoolDividerNameImpl(divider, theme);
            layer.add(divider.nameImpl.group);
        }
    });

    if (!existingLayer) {
        stage?.add(layer);
    }

    return layer;
};

export const createPoolDividersLayer = (
    existingLayer: Layer | null,
    dividers: SwimPoolDivider[],
    width: number,
    stage: Stage | null,
    theme: Theme,
): Layer => {
    const layer =
        existingLayer ||
        new Konva.Layer({
            id: 'pool_dividers_layer',
            y: TIMELINE_HEADER_SPACING,
        });

    const { SWIMPOOL_DIVIDER_COLOR } = theme;
    dividers.forEach((divider) => {
        if (divider.lineImpl) {
            divider.lineImpl.rect.width(width);
        } else {
            divider.lineImpl = new SwimPoolDividerLineImpl(divider, width, SWIMPOOL_DIVIDER_COLOR);
            layer.add(divider.lineImpl.group);
        }
    });

    if (!existingLayer) {
        stage?.add(layer);
    }

    return layer;
};

export const createGridLayer = (
    columns: GridColumn[],
    height: number,
    image: HTMLImageElement | undefined,
    todayMarker: TodayMarker,
    theme: Theme,
    zoomLevel: ZoomLevel,
): Layer => {
    const layer = new Konva.Layer({
        id: 'grid_layer',
    });

    const { TODAY_MARKER_COLOR } = theme;

    columns.forEach((column) => {
        column.columnImpl = new GridColumnImpl(column, height, image, theme, zoomLevel);
        layer.add(column.columnImpl.group);
    });

    todayMarker.lineImpl = new TodayLineImpl(todayMarker, height, TODAY_MARKER_COLOR);
    layer.add(todayMarker.lineImpl.group);

    return layer;
};

export const createHeaderLayer = (columns: GridColumn[], todayMarker: TodayMarker, theme: Theme): Layer => {
    const layer = new Konva.Layer({
        id: 'header_layer',
    });

    columns.forEach((column) => {
        column.headerImpl = new GridHeaderImpl(column, theme);
        layer.add(column.headerImpl.group);
    });

    todayMarker.markerImpl = new TodayMarkerImpl(todayMarker, theme.TODAY_MARKER_COLOR);
    layer.add(todayMarker.markerImpl.group);

    return layer;
};
