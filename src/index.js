import Resolver from '@forge/resolver';
import { initializeDefinitions } from './definitions';

const resolver = new Resolver();

initializeDefinitions(resolver);

export const handler = resolver.getDefinitions();
