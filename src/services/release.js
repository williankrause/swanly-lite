import ApiService from './api';
import IssueService from './issue';

export default class ReleaseService {
    apiService;
    issueService;

    constructor() {
        this.apiService = new ApiService();
        this.issueService = new IssueService();
    }

    getByProject(key) {
        return this.apiService.requestJiraAsUser(`/rest/api/3/project/${key}/versions`);
    }
    
    async getById(id) {
        const [release, { issues: scope }, { issues: affects }] = await Promise.all([
            this.apiService.requestJiraAsUser(`/rest/api/3/version/${id}`),
            this.issueService.getByFixVersion(id),
            this.issueService.getByAffectedVersion(id),
        ]);
        
        return {
            ...release,
            scope,
            affects,
        }
    }
}
