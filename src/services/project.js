import ApiService from './api';

export default class ProjectService {
    apiService;

    constructor() {
        this.apiService = new ApiService();
    }

    async getByKey(key) {
        return this.apiService.requestJiraAsUser(`/rest/api/3/project/${key}`);
    }
}
