import api from '@forge/api';

export default class ApiService {
    async requestJiraAsUser(path) {
        const response = await api.asUser().requestJira(path);
        const data = await response.json();

        return data;
    }

    async requestJiraAsApp(path) {
        const response = await api.asApp().requestJira(path);
        const data = await response.json();

        return data;
    }
}