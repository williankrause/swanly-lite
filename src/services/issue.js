import ApiService from './api';

export default class IssueService {
    apiService;

    constructor() {
        this.apiService = new ApiService();
    }
    
    async getByFixVersion(releaseId) {
        const jql = `fixVersion = ${releaseId} ORDER BY createdDate ASC`;

        return this.apiService.requestJiraAsUser(`/rest/api/3/search?jql=${jql}`);
    }
    
    async getByAffectedVersion(releaseId) {
        const jql = `affectedVersion = ${releaseId} ORDER BY createdDate ASC`;

        return this.apiService.requestJiraAsUser(`/rest/api/3/search?jql=${jql}`);
    }
}
