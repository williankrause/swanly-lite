import ReleaseDefinitions from './release';

export const initializeDefinitions = (resolver) => {
    new ReleaseDefinitions(resolver);
}