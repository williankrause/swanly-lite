import ProjectService from '../services/project';
import ReleaseService from '../services/release';

export default class ReleaseDefinitions {
    projectService;
    releaseService;

    constructor(resolver) {
        this.projectService = new ProjectService();
        this.releaseService = new ReleaseService();
        resolver.define('getReleases', this.getReleases);
        resolver.define('getRelease', this.getRelease);
    }

    getReleases = async ({ context }) => {
        const { key } = context.extension.project;

        const [project, releases] = await Promise.all([
            this.projectService.getByKey(key),
            this.releaseService.getByProject(key),
        ]);
    
        return { project, releases };
    }
    
    getRelease = async ({ payload }) => {
        return this.releaseService.getById(payload.id);
    }
}